@extends('fe.layout.master')
@section('title')
    Giỏ hàng
@endsection
@section('main')
    <main class="site-main  main-container no-sidebar">
        <div class="container">
            <div class="row">
                <div class="main-content col-md-12">
                    <div class="page-main-content">
                        <div class="akasha">
                            <div class="akasha-notices-wrapper"></div>
                            <form action="{{route('frontend.orderPost')}}" method="post" class="checkout akasha-checkout"
                                enctype="multipart/form-data" novalidate="novalidate">
                                @csrf
                                <div class="col2-set" id="customer_details">
                                    <div class="col-1">
                                        <div class="akasha-billing-fields">
                                            <h3>Chi tiết hóa đơn</h3>
                                            <div class="akasha-billing-fields__field-wrapper">
                                                <p class="form-row form-row-wide" id="billing_company_field"
                                                    data-priority="30"><label for="billing_company"
                                                        class="">Họ và tên</span></label><span
                                                        class="akasha-input-wrapper"><input type="text"
                                                            class="input-text " name="name"
                                                            id="billing_company" placeholder="" value=""
                                                            autocomplete="organization"></span>
                                                </p>
                                                <p class="form-row form-row-wide" id="billing_company_field"
                                                    data-priority="30"><label for="billing_company"
                                                        class="">Số điện thoại</span></label><span
                                                        class="akasha-input-wrapper"><input type="text"
                                                            class="input-text " name="phone"
                                                            id="billing_company" placeholder="" value=""
                                                            autocomplete="organization"></span>
                                                </p>
                                                <p class="form-row form-row-wide" id="billing_company_field"
                                                    data-priority="30"><label for="billing_company"
                                                        class="">Email</span></label><span
                                                        class="akasha-input-wrapper"><input type="text"
                                                            class="input-text " name="email"
                                                            id="billing_company" placeholder="" value=""
                                                            autocomplete="organization"></span>
                                                </p>
                                                <p class="form-row form-row-wide" id="billing_company_field"
                                                    data-priority="30"><label for="billing_company"
                                                        class="">Địa chỉ</span></label><span
                                                        class="akasha-input-wrapper"><input type="text"
                                                            class="input-text " name="address"
                                                            id="billing_company" placeholder="" value=""
                                                            autocomplete="organization"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="akasha-shipping-fields">
                                        </div>
                                        <div class="akasha-additional-fields">
                                            <h3>Thông tin thêm</h3>
                                            <div class="akasha-additional-fields__field-wrapper">
                                                <p class="form-row notes" id="order_comments_field" data-priority=""><label
                                                        for="order_comments" class="">Ghi chú đơn hàng</label><span
                                                        class="akasha-input-wrapper">
                                                        <textarea name="order_comments" class="input-text " id="order_comments" name="note"
                                                            placeholder="Ghi chú về đơn đặt hàng của bạn, ví dụ: những lưu ý đặc biệt khi giao hàng."
                                                            rows="2" cols="5"></textarea>
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3 id="order_review_heading">Đơn đặt hàng của bạn</h3>
                                <div id="order_review" class="akasha-checkout-review-order">
                                    <table class="shop_table akasha-checkout-review-order-table">
                                        <thead>
                                            <tr>
                                                <th class="product-name">Sản phẩm</th>
                                                <th class="product-total">Tổng tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (Cart::Content() as $item)
                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    {{ $item->name }}&nbsp;&nbsp; <strong class="product-quantity">×
                                                        {{ $item->qty }}</strong></td>
                                                <td class="product-total">
                                                    <span class="akasha-Price-amount amount"><span
                                                            class="akasha-Price-currencySymbol"></span>{{ number_format($item->price * $item->qty, 0, '', ',') }} đ</span>
                                                </td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                        <tfoot>
                                            <tr class="order-total">
                                                <th>Tổng hóa đơn:</th>
                                                <td><strong><span class="akasha-Price-amount amount"><span
                                                                class="akasha-Price-currencySymbol"></span>{{ number_format(str_replace(',', '', Cart::subtotal()), 0, '', ',') }} đ</span></strong>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <input type="hidden" name="lang" value="en">
                                    <div id="payment" class="akasha-checkout-payment">
                                        <div class="form-row place-order">
                                            <button type="submit" class="button alt" name="akasha_checkout_place_order"
                                                id="place_order" value="Place order" data-value="Place order">Đặt hàng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
