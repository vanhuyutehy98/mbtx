@extends('fe.layout.master')
@section('title')
    Đặt hàng thành công
@endsection
@section('main')
<div class="main-container text-center error-404 not-found">
    <div class="container">
        <h1 class="title-404">Đặt hành thành công</h1>
        <h1 class="title">Chúng tôi đã nhận được đơn hàng của bạn, chúng tôi sẽ liên hệ xác nhận đơn hàng sớm nhất có thể !</h1>
        <!-- .page-content -->
        <a href="{{route('frontend.index')}}" class="button">Tiếp tục mua sắm</a>
    </div>
</div>
@endsection
