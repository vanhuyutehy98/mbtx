@extends('fe.layout.master')
@section('title')
    Cửa hàng
@endsection
@section('main')
<div class="main-container shop-page right-sidebar">
    <div class="container">
        <div class="row">
            <div class="main-content col-xl-9 col-lg-8 col-md-8 col-sm-12 has-sidebar">
                <div class=" auto-clear equal-container better-height akasha-products">
                    <ul class="row products columns-3">
                        @foreach ($products as $item)
                        <li class="product-item wow fadeInUp product-item rows-space-30 col-bg-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-ts-6 style-01 post-24 product type-product status-publish has-post-thumbnail product_cat-chair product_cat-table product_cat-new-arrivals product_tag-light product_tag-hat product_tag-sock first instock featured shipping-taxable purchasable product-type-variable has-default-attributes"
                            data-wow-duration="1s" data-wow-delay="0ms" data-wow="fadeInUp">
                            <div class="product-inner tooltip-left">
                                <div class="product-thumb">
                                    <a class="thumb-link" href="{{route('frontend.detail',['slug'=>$item->slug])}}">
                                        @if ($item->productdetail->count() > 0)
                                            <img class="img-responsive"
                                            src="{{asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                            alt="Women Bags" style="width: 262px;height: 335px;">
                                        @else
                                            <img src="{{asset('images/no-img.jpg') }}"
                                            alt="Women Bags" style="width: 262px;height: 335px;">
                                        @endif
                                        
                                    </a>
                                    <div class="flash">
                                        <span class="onnew"><span class="text">New</span></span></div>
                                    <div class="group-button">
                                        <div class="add-to-cart">
                                            <a href="{{route('addcartshop',['slug'=>$item->slug])}}" class="button product_type_variable add_to_cart_button"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-info equal-elem">
                                    <h3 class="product-name product_title">
                                        <a href="{{route('frontend.detail',['slug'=>$item->slug])}}">{{$item->name}}</a>
                                    </h3>
                                    <div class="rating-wapper ">
                                        <div class="star-rating">
                                            <span style="width:100%">Rated 
                                                <strong class="rating">5.00</strong> out of 5
                                            </span>
                                        </div>
                                        <span class="review">(1)</span>

                                    </div>
                                    <span class="price"> <span
                                            class="akasha-Price-amount amount"><span
                                            class="akasha-Price-currencySymbol"></span>Giá chỉ từ: {{number_format($item->price)}} đ</span></span>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="shop-control shop-after-control">
                    {{ $products->links() }}
                </div>
            </div>
            <div class="sidebar col-xl-3 col-lg-4 col-md-4 col-sm-12">
                <div id="widget-area" class="widget-area shop-sidebar">
                    <div id="akasha_product_search-2" class="widget akasha widget_product_search">
                        <form method="get" class="akasha-product-search">
                            <input id="akasha-product-search-field-0" class="search-field"
                                   placeholder="Tìm..." value="" name="search" type="search">
                            <button type="submit" value="Search">Tìm kiếm</button>
                        </form>
                    </div>
                    <div id="akasha_price_filter-2" class="widget akasha widget_price_filter"><h2
                            class="widgettitle">Tìm kiếm theo khoảng giá<span class="arrow"></span></h2>
                        <form method="get">
                            <div class="price_slider_wrapper">

                                <label for="amount">Giá từ: </label>
                                <p>
                                    <input class="form-control" type="text" id="amount_start" name="start_price"
                                        value="1" style="margin-bottom: 10px;">
                                    <input class="form-control" type="text" id="amount_end" name="end_price"
                                        value="80000000">
                                </p>
                                <div id="slider-range"></div><br>
                                <input type="submit" data-inline="true" value="Tìm" name="submit_price">
                            </div>
                        </form>
                    </div>
                    <div id="akasha_product_categories-3" class="widget akasha widget_product_categories"><h2
                            class="widgettitle">Sản phẩm theo danh mục<span class="arrow"></span></h2>
                        <ul class="product-categories">
                            @foreach ($categories as $item)
                            <li class="cat-item cat-item-22">
                                <a href="{{route('frontend.shop', ['cat' => $item->slug])}}">{{$item->name}}</a>
                                <span class="count">({{$item->products->count()}})</span>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div><!-- .widget-area -->
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $("#slider-range").slider({
            range: true,
            min: 1,
            max: 80000000,
            values: [1, 80000000],
            slide: function(event, ui) {
                $("#amount_start").val(ui.values[0]);
                $("#amount_end").val(ui.values[1]);
            }
        });
    });

</script>
@endpush
