@extends('fe.layout.master')
@section('title')
    Bài viết
@endsection
@section('main')
    <div class="main-container no-sidebar">
        <div>
            <div class="container">
                <div class="akasha-heading style-01">
                    <div class="heading-inner">
                        <h3 class="title">
                            Danh sách bài viết</h3>
                        <div class="subtitle">
                            Danh sách bài viết mới nhất được cập nhật tại đây
                        </div>
                    </div>
                </div>
                <div class="akasha-blog style-01">
                    <div class="blog-list-grid row auto-clear equal-container better-height ">
                        @foreach ($posts as $item)
                        <article class="post-item post-grid rows-space-30 col-bg-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-ts-6 post-195 post type-post status-publish format-standard has-post-thumbnail hentry category-light category-table category-life-style tag-light tag-life-style">
                            <div class="post-inner blog-grid">
                                <div class="post-thumb">
                                    <a href="{{route('frontend.postDetail',['id'=>$item->id])}}">
                                        <img src="../blogs/{{ $item->image }}" class="img-responsive attachment-370x330 size-370x330" alt="img" width="370" height="330"> </a>
                                   
                                </div>
                                <div class="post-content">
                                    <div class="post-meta">
                                        <div class="post-author">
                                            By:<a href="#">
                                            admin </a>
                                        </div>
                                        <div class="post-comment-icon">
                                            <a href="#">
                                                {{ $item->commentBlog->count() }} </a>
                                        </div>
                                    </div>
                                    <div class="post-info equal-elem" style="height: 141px;">
                                        <h2 class="post-title"><a href="#">{{$item->info}}.</a></h2>{!!substr($item->body, 0, 50)!!}...
                                    </div>
                                </div>
                            </div>
                        </article>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
