@extends('fe.layout.master')
@section('title')
    Giỏ hàng
@endsection
@section('main')
<main class="site-main main-container no-sidebar">
    <div class="container">
        <div class="row">
            <div class="main-content col-md-12">
                <div class="page-main-content">
                    <div class="akasha">
                        <div class="akasha-notices-wrapper"></div>
                        <form class="akasha-cart-form">
                            <table class="shop_table shop_table_responsive cart akasha-cart-form__contents"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="product-remove">&nbsp;</th>
                                    <th class="product-thumbnail">&nbsp;</th>
                                    <th class="product-name">Tên sản phẩm</th>
                                    <th class="product-price">Giá</th>
                                    <th class="product-quantity">Số lượng</th>
                                    <th class="product-subtotal">Tổng tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach (Cart::Content() as $item)
                                        <tr class="akasha-cart-form__cart-item cart_item">
                                            <td class="product-remove">
                                                <a href="{{ route('removecart', ['id' => $item->rowId]) }}" onclick="return del_cart('{{ $item->name }}')"
                                                class="remove" aria-label="Remove this item" data-product_id="27"
                                                data-product_sku="885B712">×</a></td>
                                            <td class="product-thumbnail">
                                                <a href="#"><img
                                                        src="{{asset('images/product/') }}/{{ $item->options->img }}"
                                                        class="attachment-akasha_thumbnail size-akasha_thumbnail"
                                                        alt="img" style="width: 80px;height: 102px;"></a></td>
                                            <td class="product-name" data-title="Product">
                                                <a href="#">{{ $item->name }}</a></td>
                                            <td class="product-price" data-title="Price">
                                                <span class="akasha-Price-amount amount"><span
                                                        class="akasha-Price-currencySymbol"></span>{{ number_format($item->price, 0, '', ',') }} đ</span></td>
                                            <td class="product-quantity" data-title="Quantity">
                                                <div class="quantity">
                                                    <span class="qty-label">Số lượng:</span>
                                                    <div class="control">
                                                        <input onchange="update_qty('{{ $item->rowId }}',this.value)"
                                                            type="number" id="quantity" name="quantity"
                                                            class="input-qty input-text qty text"
                                                            value="{{ $item->qty }}">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="product-subtotal" data-title="Total">
                                                <span class="akasha-Price-amount amount"><span
                                                        class="akasha-Price-currencySymbol"></span>{{ number_format($item->price * $item->qty, 0, '', ',') }} đ</span></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                        <div class="cart-collaterals">
                            <div class="cart_totals ">
                                <h2>Cart totals</h2>
                                <table class="shop_table shop_table_responsive" cellspacing="0">
                                    <tbody>
                                    <tr class="order-total">
                                        <th>Tổng</th>
                                        <td data-title="Total"><strong><span
                                                class="akasha-Price-amount amount"><span
                                                class="akasha-Price-currencySymbol"></span>{{ number_format(str_replace(',', '', Cart::subtotal()), 0, '', ',') }} đ</span></strong>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="akasha-proceed-to-checkout">
                                    <a href="{{route('frontend.order')}}"
                                       class="checkout-button button alt akasha-forward">
                                        Thanh toán đơn hàng</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 dreaming_crosssell-product">
                            <div class="block-title">
                                <h2 class="product-grid-title">
                                    <span>Sản phẩm mới</span>
                                </h2>
                            </div>
                            <div class="owl-slick owl-products equal-container better-height"
                                 data-slick="{&quot;arrows&quot;:false,&quot;slidesMargin&quot;:30,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;slidesToShow&quot;:4}"
                                 data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;30&quot;}}]">
                                
                                @foreach ($product_new as $item)
                                <div class="product-item style-01 post-278 page type-page status-publish hentry">
                                    <div class="product-inner tooltip-left">
                                        <div class="product-thumb">
                                            <a class="thumb-link"
                                               href="{{route('frontend.detail',['slug'=>$item->slug])}}"
                                               tabindex="0">
                                                @if ($item->productdetail->count() > 0)
                                                    <img class="img-responsive"
                                                    src="{{asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                                    alt="Print In White" style="width: 262px;height: 335px;">
                                                @else
                                                    <img src="{{asset('images/no-img.jpg') }}"
                                                    style="width: 262px;height: 335px;">
                                               @endif
                                                
                                            </a>
                                            <div class="group-button">
                                                <div class="add-to-cart">
                                                    <a href="{{route('addcartshop',['slug'=>$item->slug])}}"
                                                       class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm giỏ hàng
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-info equal-elem">
                                            <h3 class="product-name product_title">
                                                <a href="{{route('frontend.detail',['slug'=>$item->slug])}}"
                                                   tabindex="0">{{$item->name}}</a>
                                            </h3>
                                            
                                            <span class="price">
                                            <ins>
                                                <span class="akasha-Price-amount amount">
                                                    <span class="akasha-Price-currencySymbol">
                                                    </span>Giá chỉ từ: {{number_format($item->price)}} đ</span>
                                                </ins>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    function del_cart(name) {
        return confirm('Bạn muốn xóa khỏi giỏ hàng: ' + name + ' ?');
    }
    function update_qty(rowId, qty) {
        $.get('/updatecart/' + rowId + '/' + qty,
            function() {
                window.location.reload();
            }
        );
    }

</script>
@endsection
