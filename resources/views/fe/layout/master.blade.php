<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/assets/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/animate.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/chosen.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/jquery.scrollbar.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/lightbox.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/magnific-popup.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/slick.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/fonts/flaticon.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/megamenu.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/dreaming-attribute.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/style.css') }}"/>
    <title>Akasha - HTML Template </title>
</head>
<body>
<header id="header" class="header style-02 header-dark header-transparent">
    <div class="header-wrap-stick">
        <div class="header-position">
            <div class="header-middle">
                <div class="akasha-menu-wapper"></div>
                <div class="header-middle-inner">
                    <div class="header-search-menu">
                        <div class="block-menu-bar">
                            <a class="menu-bar menu-toggle" href="#">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>
                    <div class="header-logo-nav">
                        <div class="header-logo">
                            <a href="{{route('frontend.index')}}">
                                <img alt="Akasha" src="{{asset('images/logo/') }}/{{ $setting[0]->logo }}" class="logo">
                            </a>
                        </div>
                        <div class="box-header-nav menu-nocenter">
                            <ul id="menu-primary-menu"
                                class="clone-main-menu akasha-clone-mobile-menu akasha-nav main-menu">
                                <li id="menu-item-230"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-230 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="akasha-menu-item-title" title="Home" href="{{route('frontend.index')}}">Trang chủ</a>
                                </li>
                                <li id="menu-item-228"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-237 parent">
                                    <a class="akasha-menu-item-title" title="Pages" href="{{route('frontend.shop')}}">Cửa hàng</a>
                                    <span class="toggle-submenu"></span>
                                    <ul role="menu" class="submenu">
                                        @foreach ($categories as $item)
                                        <li id="menu-item-987"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-987">
                                            <a class="akasha-menu-item-title" title=""
                                            href="{{route('frontend.shop', ['cat'=>$item->slug])}}">{{$item->name}}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li id="menu-item-237"
                                    class="menu-item menu-item-type-post_type menu-item-object-megamenu menu-item-228 parent parent-megamenu item-megamenu menu-item-has-children">
                                    <a class="akasha-menu-item-title" title="Shop"
                                       href="{{route('frontend.post')}}">Bài viết</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="header-control">
                        <div class="header-control-inner">
                            <div class="meta-dreaming">
                                <div class="header-search akasha-dropdown">
                                    <div class="header-search-inner" data-akasha="akasha-dropdown">
                                        <a href="#" class="link-dropdown block-link">
                                            <span class="flaticon-magnifying-glass-1"></span>
                                        </a>
                                    </div>
                                    <div class="block-search">
                                        <form action="{{route('frontend.shop')}}" role="search" method="get"
                                              class="form-search block-search-form akasha-live-search-form">
                                            <div class="form-content search-box results-search">
                                                <div class="inner">
                                                    <input autocomplete="off" class="searchfield txt-livesearch input"
                                                           name="search" value="" placeholder="Tìm sản phẩm..." type="text">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn-submit">
                                                <span class="flaticon-magnifying-glass-1"></span>
                                            </button>
                                        </form><!-- block search -->
                                    </div>
                                </div>
                                <div class="akasha-dropdown-close">x</div>
                                <div class="block-minicart block-dreaming akasha-mini-cart akasha-dropdown">
                                    <div class="shopcart-dropdown block-cart-link" data-akasha="akasha-dropdown">
                                        <a class="block-link link-dropdown" href="cart.html">
                                            <span class="flaticon-bag"></span>
                                            <span class="count">{{ Cart::Content()->count() }}</span>
                                        </a>
                                    </div>
                                    <div class="widget akasha widget_shopping_cart">
                                        <div class="widget_shopping_cart_content">
                                            <h3 class="minicart-title">Đơn hàng của bạn<span class="minicart-number-items">{{ Cart::Content()->count() }}</span></h3>
                                            <ul class="akasha-mini-cart cart_list product_list_widget">
                                                @foreach (Cart::Content() as $item)
                                                    <li class="akasha-mini-cart-item mini_cart_item">
                                                        <a href="{{ route('removecart', ['id' => $item->rowId]) }}" class="remove remove_from_cart_button">×</a>
                                                        <a href="{{route('frontend.detail',['slug'=>$item->options->slug])}}">
                                                            <img src="{{asset('images/product/') }}/{{ $item->options->img }}"
                                                                class="attachment-akasha_thumbnail size-akasha_thumbnail"
                                                                alt="img" width="600" height="778">{{ $item->name }}&nbsp;
                                                        </a>
                                                        <span class="quantity">{{ $item->qty }} × <span
                                                                class="akasha-Price-amount amount"><span
                                                                class="akasha-Price-currencySymbol"></span>{{ number_format($item->price * $item->qty, 0, '', ',') }} đ</span></span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <p class="akasha-mini-cart__total total"><strong>Tổng:</strong>
                                                <span class="akasha-Price-amount amount"><span
                                                        class="akasha-Price-currencySymbol"></span>{{ number_format(str_replace(',', '', Cart::subtotal()), 0, '', ',') }} đ</span>
                                            </p>
                                            <p class="akasha-mini-cart__buttons buttons">
                                                <a href="{{route('frontend.buy')}}" class="button akasha-forward">Xem giỏ hàng</a>
                                                <a href="{{route('frontend.order')}}" class="button checkout akasha-forward">Thanh toán</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile">
        <div class="header-mobile-left">
            <div class="block-menu-bar">
                <a class="menu-bar menu-toggle" href="#">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <div class="header-search akasha-dropdown">
                <div class="header-search-inner" data-akasha="akasha-dropdown">
                    <a href="#" class="link-dropdown block-link">
                        <span class="flaticon-magnifying-glass-1"></span>
                    </a>
                </div>
                <div class="block-search">
                    <form role="search" method="get"
                          class="form-search block-search-form akasha-live-search-form">
                        <div class="form-content search-box results-search">
                            <div class="inner">
                                <input autocomplete="off" class="searchfield txt-livesearch input" name="s" value=""
                                       placeholder="Search here..." type="text">
                            </div>
                        </div>
                        <input name="post_type" value="product" type="hidden">
                        <input name="taxonomy" value="product_cat" type="hidden">
                        <button type="submit" class="btn-submit">
                            <span class="flaticon-magnifying-glass-1"></span>
                        </button>
                    </form><!-- block search -->
                </div>
            </div>
            <ul class="wpml-menu">
                <li class="menu-item akasha-dropdown block-language">
                    <a href="#" data-akasha="akasha-dropdown">
                        <img src="{{asset('frontend/assets/images/en.png') }}"
                             alt="en" width="18" height="12">
                        English
                    </a>
                    <span class="toggle-submenu"></span>
                    <ul class="sub-menu">
                        <li class="menu-item">
                            <a href="#">
                                <img src="{{asset('frontend/assets/images/it.png') }}"
                                     alt="it" width="18" height="12">
                                Italiano
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item">
                    <div class="wcml-dropdown product wcml_currency_switcher">
                        <ul>
                            <li class="wcml-cs-active-currency">
                                <a class="wcml-cs-item-toggle">USD</a>
                                <ul class="wcml-cs-submenu">
                                    <li>
                                        <a>EUR</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="header-mobile-mid">
            <div class="header-logo">
                <a href="{{route('frontend.index')}}"><img alt="Akasha" src="{{asset('frontend/assets/images/logo.png') }}" class="logo"></a>
            </div>
        </div>
        <div class="header-mobile-right">
            <div class="header-control-inner">
                <div class="meta-dreaming">
                    <div class="menu-item block-user block-dreaming akasha-dropdown">
                        <a class="block-link" href="my-account.html">
                            <span class="flaticon-profile"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="menu-item akasha-MyAccount-navigation-link akasha-MyAccount-navigation-link--dashboard is-active">
                                <a href="#">Dashboard</a>
                            </li>
                            <li class="menu-item akasha-MyAccount-navigation-link akasha-MyAccount-navigation-link--orders">
                                <a href="#">Orders</a>
                            </li>
                            <li class="menu-item akasha-MyAccount-navigation-link akasha-MyAccount-navigation-link--downloads">
                                <a href="#">Downloads</a>
                            </li>
                            <li class="menu-item akasha-MyAccount-navigation-link akasha-MyAccount-navigation-link--edit-adchair">
                                <a href="#">Addresses</a>
                            </li>
                            <li class="menu-item akasha-MyAccount-navigation-link akasha-MyAccount-navigation-link--edit-account">
                                <a href="#">Account details</a>
                            </li>
                            <li class="menu-item akasha-MyAccount-navigation-link akasha-MyAccount-navigation-link--customer-logout">
                                <a href="#">Logout</a>
                            </li>
                        </ul>
                    </div>
                    <div class="block-minicart block-dreaming akasha-mini-cart akasha-dropdown">
                        <div class="shopcart-dropdown block-cart-link" data-akasha="akasha-dropdown">
                            <a class="block-link link-dropdown" href="cart.html">
                                <span class="flaticon-bag"></span>
                                <span class="count">3</span>
                            </a>
                        </div>
                        <div class="widget akasha widget_shopping_cart">
                            <div class="widget_shopping_cart_content">
                                <h3 class="minicart-title">Your Cart<span class="minicart-number-items">3</span></h3>
                                <ul class="akasha-mini-cart cart_list product_list_widget">
                                    <li class="akasha-mini-cart-item mini_cart_item">
                                        <a href="#" class="remove remove_from_cart_button">×</a>
                                        <a href="#">
                                            <img src="{{asset('frontend/assets/images/apro134-1-600x778.jpg') }}"
                                                 class="attachment-akasha_thumbnail size-akasha_thumbnail"
                                                 alt="img" width="600" height="778">T-shirt with skirt – Pink&nbsp;
                                        </a>
                                        <span class="quantity">1 × <span
                                                class="akasha-Price-amount amount"><span
                                                class="akasha-Price-currencySymbol">$</span>150.00</span></span>
                                    </li>
                                    <li class="akasha-mini-cart-item mini_cart_item">
                                        <a href="#" class="remove remove_from_cart_button">×</a>
                                        <a href="#">
                                            <img src="{{asset('frontend/assets/images/apro1113-600x778.jpg') }}"
                                                 class="attachment-akasha_thumbnail size-akasha_thumbnail"
                                                 alt="img" width="600" height="778">Abstract Sweatshirt&nbsp;
                                        </a>
                                        <span class="quantity">1 × <span
                                                class="akasha-Price-amount amount"><span
                                                class="akasha-Price-currencySymbol">$</span>129.00</span></span>
                                    </li>
                                    <li class="akasha-mini-cart-item mini_cart_item">
                                        <a href="#" class="remove remove_from_cart_button">×</a>
                                        <a href="#">
                                            <img src="{{asset('frontend/assets/images/apro201-1-600x778.jpg') }}"
                                                 class="attachment-akasha_thumbnail size-akasha_thumbnail"
                                                 alt="img" width="600" height="778">Mini Dress&nbsp;
                                        </a>
                                        <span class="quantity">1 × <span
                                                class="akasha-Price-amount amount"><span
                                                class="akasha-Price-currencySymbol">$</span>139.00</span></span>
                                    </li>
                                </ul>
                                <p class="akasha-mini-cart__total total"><strong>Subtotal:</strong>
                                    <span class="akasha-Price-amount amount"><span
                                            class="akasha-Price-currencySymbol">$</span>418.00</span>
                                </p>
                                <p class="akasha-mini-cart__buttons buttons">
                                    <a href="cart.html" class="button akasha-forward">Viewcart</a>
                                    <a href="checkout.html" class="button checkout akasha-forward">Checkout</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="fullwidth-template">
    <div class="slide-home-02">
        <div class="response-product product-list-owl owl-slick equal-container better-height"
             data-slick="{&quot;arrows&quot;:false,&quot;slidesMargin&quot;:0,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:1,&quot;rows&quot;:1}"
             data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;0&quot;}}]">
            @foreach ($banners as $item)
            <div class="slide-wrap">
                <a href="{{route('frontend.shop',['cat'=>$item->Categories->slug])}}">
                    <img src="{{asset('images/slides/') }}/{{$item->image}}" alt="image">
                </a>
            </div>
            @endforeach
             
                {{-- <img src="{{asset('frontend/assets/images/slide21.jpg') }}" alt="image">
                <div class="slide-info">
                    <div class="container">
                        <div class="slide-inner">
                            <h5>Sale up to</h5>
                            <h1>40<span><span>%</span><span>Off</span></span></h1>
                            <h2>This Week Only</h2>
                            <a href="#">Shop now</a>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
    @yield('main')
</div>
<footer id="footer" class="footer style-02">
    <div class="section-015">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-4">
                    <div class="logo-footer">
                        <img src="{{asset('images/logo/') }}/{{ $setting[0]->logo }}"
                             class="az_single_image-img attachment-full" alt="img" width="150" height="35">
                    </div>
                    <div class="akasha-listitem style-02">
                        <div class="listitem-inner">
                            <ul class="listitem-list">
                                <li>
                                    <span class="icon"><span class="flaticon-localization"></span></span>
                                    Cầu giấy, Hà Nội
                                </li>
                                <li>
                                    <a href="mailto:contact@yourcompany.com" target="_self">
                                        <span class="icon"><span class="flaticon-email"></span></span>
                                        tiennguyenhc@gmail.com </a>
                                </li>
                                <li>
                                    <span class="icon"><span class="flaticon-telephone"></span></span>
                                    099.999.999
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="akasha-socials style-03">
                        <div class="content-socials">
                            <ul class="socials-list">
                                <li>
                                    <a href="https://facebook.com/" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.pinterest.com/" target="_blank">
                                        <i class="fa fa-pinterest-p"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-2">
                    <div class="akasha-listitem style-02 ">
                        <div class="listitem-inner">
                            <h4 class="title">
                                Khách hàng </h4>
                            <ul class="listitem-list">
                                <li>
                                    <a href="#" target=" _blank">
                                        Đang chuyển hàng &amp; Returns </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Mua sắm an toàn </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Tình trạng đặt hàng </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Vận chuyển quốc tế </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Chi nhánh </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-2">
                    <div class="akasha-listitem style-02">
                        <div class="listitem-inner">
                            <h4 class="title">
                                Thông tin </h4>
                            <ul class="listitem-list">
                                <li>
                                    <a href="#" target="_self">
                                        Chính sách bảo mật </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Dịch vụ khách hàng </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Đơn hàng và Trả hàng </a>
                                </li>
                                <li>
                                    <a href="#" target="_self">
                                        Liên lạc </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="akasha-newsletter style-02">
                        <div class="newsletter-inner">
                            <div class="newsletter-info">
                                <div class="newsletter-wrap">
                                    <h3 class="title">Bản tin</h3>
                                    <h4 class="subtitle">Nhận chiết khấu 30%</h4>
                                    <p class="desc"></p>
                                </div>
                            </div>
                            <div class="newsletter-form-wrap">
                                <div class="newsletter-form-inner">
                                    <input class="email email-newsletter" name="email"
                                           placeholder="Enter your email ..." type="email">
                                    <a href="#" class="button btn-submit submit-newsletter">
                                        <span class="text">Đặt mua</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-016">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>© Copyright 2022 <a href="#">Akasha</a>. All Rights Reserved.</p>
                </div>
                <div class="col-md-6">
                    {{-- <div class="payment">
                        <img src="{{asset('frontend/assets/images/payment.png') }}"
                             class="az_single_image-img attachment-full" alt="img">
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer-device-mobile">
    <div class="wapper">
        <div class="footer-device-mobile-item device-home">
            <a href="index-2.html">
					<span class="icon">
						<i class="fa fa-home" aria-hidden="true"></i>
					</span>
                Home
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-wishlist">
            <a href="wishlist.html">
					<span class="icon">
						<i class="fa fa-heart" aria-hidden="true"></i>
					</span>
                Wishlist
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-cart">
            <a href="cart.html">
					<span class="icon">
						<i class="fa fa-shopping-basket" aria-hidden="true"></i>
						<span class="count-icon">
							0
						</span>
					</span>
                <span class="text">Cart</span>
            </a>
        </div>
        <div class="footer-device-mobile-item device-home device-user">
            <a href="my-account.html">
					<span class="icon">
						<i class="fa fa-user" aria-hidden="true"></i>
					</span>
                Account
            </a>
        </div>
    </div>
</div>
<a href="#" class="backtotop active">
    <i class="fa fa-angle-up"></i>
</a>
<script src="{{asset('frontend/assets/js/jquery-1.12.4.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/bootstrap.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/chosen.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/countdown.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/jquery.scrollbar.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/lightbox.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/magnific-popup.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/slick.js') }}"></script>
<script src="{{asset('frontend/assets/js/jquery.zoom.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/threesixty.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/jquery-ui.min.js') }}"></script>
<script src="{{asset('frontend/assets/js/mobilemenu.js') }}"></script>
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyC3nDHy1dARR-Pa_2jjPCjvsOR4bcILYsM'></script>
<script src="{{asset('frontend/assets/js/functions.js') }}"></script>
@stack('scripts')
</body>

<!-- Mirrored from ledthanhdat.vn/html/akasha/home-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 20 Mar 2022 04:26:55 GMT -->
</html>