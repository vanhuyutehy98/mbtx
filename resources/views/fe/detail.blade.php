@extends('fe.layout.master')
@section('title')
    Chi tiết
@endsection
@section('main')
    <div class="banner-wrapper no_background">
        <div class="banner-wrapper-inner">
            <nav class="akasha-breadcrumb"><a href="index-2.html">Trang chủ</a><i class="fa fa-angle-right"></i><a
                    href="#">Cửa hàng</a>
                <i class="fa fa-angle-right"></i>{{ $product->name }}
            </nav>
        </div>
    </div>
    <div class="single-thumb-vertical main-container shop-page no-sidebar">
        <div class="container">
            <div class="row">
                <div class="main-content col-md-12">
                    <div class="akasha-notices-wrapper"></div>
                    <div id="product-27"
                        class="post-27 product type-product status-publish has-post-thumbnail product_cat-table product_cat-new-arrivals product_cat-lamp product_tag-table product_tag-sock first instock shipping-taxable purchasable product-type-variable has-default-attributes">
                        <div class="main-contain-summary">
                            <div class="contain-left has-gallery">
                                <div class="single-left">
                                    <div
                                        class="akasha-product-gallery akasha-product-gallery--with-images akasha-product-gallery--columns-4 images">
                                        <div class="flex-viewport">
                                            <figure class="akasha-product-gallery__wrapper">
                                                @foreach ($product->productdetail as $item)
                                                    <div class="akasha-product-gallery__image">
                                                        <img alt="img"
                                                            src="{{ asset('images/product/') }}/{{ $item->image }}">
                                                    </div>
                                                @endforeach

                                            </figure>
                                        </div>
                                        <ol class="flex-control-nav flex-control-thumbs">
                                            @foreach ($product->productdetail as $item)
                                                <li><img src="{{ asset('images/product/') }}/{{ $item->image }}"
                                                        alt="img">
                                                </li>
                                            @endforeach

                                        </ol>
                                    </div>
                                </div>
                                <div class="summary entry-summary">
                                    <div class="flash">
                                        <span class="onnew"><span class="text">New</span></span>
                                    </div>
                                    <h1 class="product_title entry-title">{{ $product->name }}</h1>
                                    <p class="price">
                                        <span class="akasha-Price-amount amount">
                                            <span class="akasha-Price-currencySymbol"></span>
                                            {{ number_format($product->price) }} đ
                                        </span>
                                    </p>
                                    <div class="akasha-product-details__short-description">
                                        <p>{!! $product->info !!}</p>
                                        <ul>
                                            <li>Kiểu dáng: {{ $product->style }}</li>
                                            <li>Màu: {{ $product->color }}</li>
                                            <li>Kích thước: {{ $product->size }}</li>
                                            <li>Xuất xứ: {{ $product->origin }}</li>
                                            <li>Chất liệu: {{ $product->material }}</li>
                                        </ul>
                                    </div>
                                    <form action="{{ route('frontend.buycar') }}" method="post">
                                        @csrf
                                        <div class="quantity">
                                            <span class="qty-label">Số lượng:</span>
                                            <div class="control">
                                                <a class="btn-number qtyminus quantity-minus" href="#">-</a>
                                                <input type="text" data-step="1" min="1" max="" name="quantity" value="1"
                                                    title="Qty" class="input-qty input-text qty text" size="4"
                                                    pattern="[0-9]*" inputmode="numeric">
                                                <a class="btn-number qtyplus quantity-plus" href="#">+</a>
                                            </div>
                                        </div>
                                        <div class="variations_form cart">
                                            <input type="hidden" value="{{ $product->id }}" name="prd_id">
                                            <button type="submit" class="single_add_to_cart_button button alt">Đặt
                                                hàng</button>
                                        </div>
                                    </form>
                                    <div class="clear"></div>
                                    <div class="product_meta">
                                        <span class="sku_wrapper">Mã sản phẩm: <span
                                                class="sku">{{ $product->id }}</span></span>
                                        <span class="posted_in">Danh mục: {{ $product->Categories->name }}
                                        </span>
                                    </div>
                                    <div class="akasha-share-socials">
                                        <h5 class="social-heading">Share: </h5>
                                        <a target="_blank" class="facebook" href="javascript:void(0)">
                                            <i class="fa fa-facebook-f"></i>
                                        </a>
                                        <a target="_blank" class="twitter" href="javascript:void(0)"><i
                                                class="fa fa-twitter"></i>
                                        </a>
                                        <a target="_blank" class="pinterest" href="javascript:void(0)"> <i
                                                class="fa fa-pinterest"></i>
                                        </a>
                                        <a target="_blank" class="googleplus" href="javascript:void(0)"><i
                                                class="fa fa-google-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="akasha-tabs akasha-tabs-wrapper">
                            <ul class="tabs dreaming-tabs" role="tablist">
                                <li class="description_tab" id="tab-title-description" role="tab"
                                    aria-controls="tab-description">
                                    <a href="#tab-description">Chi tết</a>
                                </li>
                                <li class="reviews_tab active" id="tab-title-reviews" role="tab"
                                    aria-controls="tab-reviews">
                                    <a href="#tab-reviews">Bình luận ({{ $product->comments->count() }})</a>
                                </li>
                            </ul>
                            <div class="akasha-Tabs-panel akasha-Tabs-panel--description panel entry-content akasha-tab"
                                id="tab-description" role="tabpanel" aria-labelledby="tab-title-description"
                                style="display: none;">
                                <h2>Chi tết</h2>
                                <div class="container-table">
                                    {!! $product->describer !!}
                                </div>
                            </div>
                            <div class="akasha-Tabs-panel akasha-Tabs-panel--reviews panel entry-content akasha-tab"
                                id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews"
                                style="display: block;">
                                <div id="reviews" class="akasha-Reviews">
                                    @if ($product->comments->count() == 0)
                                        <div id="comments">
                                            <p class="akasha-noreviews">Hiện tại không có đánh giá nào.</p>
                                        </div>
                                    @else
                                    @foreach ($product->comments as $value)
                                    <h3 id="reply-title" class="comment-reply-title">Người bình luận: {{$value->name}}</h3>
                                    <span id="reply-title" class="comment-reply-title">Bình luận: {{$value->comment}}</span>
                                    @endforeach
                                  
                                    @endif


                                    <div id="review_form_wrapper">
                                        <div id="review_form">
                                            <div id="respond" class="comment-respond">
                                                @if ($product->comments->count() == 0)
                                                    <span id="reply-title" class="comment-reply-title">Hãy là người đầu tiên
                                                        nhận xét “{{ $product->name }}”</span>
                                                @endif
                                                <form method="POST" action="{{ route('addcmtprd', ['slug' => $product->slug]) }}"
                                                    class="comment-form">
                                                    @csrf
                                                    <input type="hidden" value="{{$product->id}}" name="prd_id">
                                                    <p class="comment-form-author">
                                                        <label for="author">Tên&nbsp;<span
                                                                class="required">*</span></label>
                                                        <input id="author" name="name" value="" size="30" required=""
                                                            type="text">
                                                    </p>
                                                    <p class="comment-form-email"><label for="email">Email&nbsp;
                                                            <span class="required">*</span></label>
                                                        <input id="email" name="email" value="" size="30" required=""
                                                            type="email">
                                                    </p>
                                                    <div class="comment-form-rating"><label for="rating"></label>
                                                        <p class="comment-form-comment"><label for="comment">Cảm nhận của
                                                                bạn&nbsp;<span class="required">*</span></label>
                                                            <textarea id="comment" name="comment" cols="45" rows="8" required=""></textarea>
                                                        </p><input name="wpml_language_code" value="en" type="hidden">
                                                        <p class="form-submit"><input name="submit" id="submit"
                                                                class="submit" value="Bình luận" type="submit">
                                                            <input name="comment_post_ID" value="27" id="comment_post_ID"
                                                                type="hidden">
                                                            <input name="comment_parent" id="comment_parent" value="0"
                                                                type="hidden">
                                                        </p>
                                                </form>
                                            </div><!-- #respond -->
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 dreaming_related-product">
                    <div class="block-title">
                        <h2 class="product-grid-title">
                            <span>Những sảm phẩm tương tự</span>
                        </h2>
                    </div>
                    <div class="owl-slick owl-products equal-container better-height"
                        data-slick="{&quot;arrows&quot;:false,&quot;slidesMargin&quot;:30,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;slidesToShow&quot;:4}"
                        data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;30&quot;}}]">
                        @foreach ($relatedProducts as $item)
                        <div class="product-item style-01 post-278 page type-page status-publish hentry">
                            <div class="product-inner tooltip-left">
                                <div class="product-thumb">
                                    <a class="thumb-link"
                                       href="{{route('frontend.detail',['slug'=>$item->slug])}}"
                                       tabindex="0">
                                        @if ($item->productdetail->count() > 0)
                                            <img class="img-responsive"
                                            src="{{asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                            alt="Print In White" style="width: 262px;height: 335px;">
                                        @else
                                            <img src="{{asset('images/no-img.jpg') }}"
                                            style="width: 262px;height: 335px;">
                                       @endif
                                        
                                    </a>
                                    <div class="group-button">
                                        <div class="add-to-cart">
                                            <a href="{{route('addcartshop',['slug'=>$item->slug])}}"
                                               class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm giỏ hàng
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-info equal-elem">
                                    <h3 class="product-name product_title">
                                        <a href="{{route('frontend.detail',['slug'=>$item->slug])}}"
                                           tabindex="0">{{$item->name}}</a>
                                    </h3>
                                    
                                    <span class="price">
                                    <ins>
                                        <span class="akasha-Price-amount amount">
                                            <span class="akasha-Price-currencySymbol">
                                            </span>Giá chỉ từ: {{number_format($item->price)}} đ</span>
                                        </ins>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="backtotop active">
        <i class="fa fa-angle-up"></i>
    </a>
@endsection
@push('scripts')
    <script src="{{ asset('frontend/assets/js/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/chosen.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/countdown.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/lightbox.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/slick.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery.zoom.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/threesixty.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/mobilemenu.js') }}"></script>
    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyC3nDHy1dARR-Pa_2jjPCjvsOR4bcILYsM'></script>
    <script src="{{ asset('frontend/assets/js/functions.js') }}"></script>
@endpush
