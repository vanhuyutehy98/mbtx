@extends('fe.layout.master')
@section('title')
    Chi tiết bài viết
@endsection
@section('main')
<div class="main-container no-sidebar">
    <!-- POST LAYOUT -->
    <div class="container">
        <div class="row">
            <div class="main-content col-md-12 col-sm-12">
                <div class="blog-standard content-post">
                    <article class="post-item post-standard post-195 post type-post status-publish format-standard has-post-thumbnail hentry category-light category-table category-life-style tag-light tag-life-style">
                        <div class="post-thumb">
                            <a href="#"><img src="../blogs/{{ $post->image }}" class="img-responsive attachment-1170x768 size-1170x768" alt="img" width="1170" height="768"></a></div>
                        <div class="post-info">
                            <h2 class="post-title"><a href="#">{{ $post->info }}</a></h2>
                            <div class="post-meta">
                                <div class="date">
                                    <a href="#">
                                       {{$created_at}} </a>
                                </div>
                                <div class="post-author">
                                    By:<a href="#">
                                    admin </a>
                                </div>
                            </div>
                        </div>
                        <div class="post-content">
                            {!!$post->body!!}
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
