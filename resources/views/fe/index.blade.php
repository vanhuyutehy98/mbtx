@extends('fe.layout.master')
@section('title')
    Trang chủ
@endsection
@section('main')
    <div class="section-001 section-004">
        <div class="container">
            <div class="row">
                @foreach ($category_home as $item)
                    <div class="col-md-12 col-lg-6">
                        <div class="akasha-banner style-04 left-center">
                            <div class="banner-inner">
                                <figure class="banner-thumb">
                                    <img src="{{ asset('images/category/') }}/{{ $item->image }}"
                                        class="attachment-full size-full" alt="img">
                                </figure>
                                <div class="banner-info ">
                                    <div class="banner-content">
                                        <div class="title-wrap">
                                            <h6 class="title">Bán chạy </h6>
                                        </div>
                                        <div class="cate">Akasha</div>
                                        <div class="button-wrap">
                                            <div class="subtitle">Ưu đãi trong tuần</div>
                                            <a class="button" target="_self"
                                                href="{{ route('frontend.shop', ['cat' => $item->slug]) }}"><span>Xem ngay</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
     @endforeach

                                        </div>
                                    </div>
                                </div>
                                <div class="section-011">
                                    <div class="container">
                                        <div class="akasha-heading style-01">
                                            <div class="heading-inner">
                                                <h3 class="title">
                                                    Sản phẩm mới </h3>
                                                <div class="subtitle">
                                                    Sản phẩm mới nhất của tuần thời trang Paris
                                                </div>
                                            </div>
                                        </div>
                                        <div class="akasha-products style-04">
                                            <div class="response-product product-list-owl owl-slick equal-container better-height"
                                                data-slick="{&quot;arrows&quot;:true,&quot;slidesMargin&quot;:30,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:4,&quot;rows&quot;:1}"
                                                data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesMargin&quot;:&quot;30&quot;}}]">
                                                @foreach ($product_new as $item)
                                                    <div
                                                        class="product-item recent-product style-04 rows-space-0 post-93 product type-product status-publish has-post-thumbnail product_cat-light product_cat-table product_cat-new-arrivals product_tag-table product_tag-sock first instock shipping-taxable purchasable product-type-simple  ">
                                                        <div class="product-inner tooltip-top tooltip-all-top">
                                                            <div class="product-thumb">
                                                                <a class="thumb-link"
                                                                    href="{{ route('frontend.detail', ['slug' => $item->slug]) }}"
                                                                    tabindex="0">
                                                                    @if ($item->productdetail->count() > 0)
                                                                        <img class="img-responsive"
                                                                            src="{{ asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                                                            alt="Black Shirt" width="270px" height="350px">
                                                                    @else
                                                                        <img src="{{ asset('images/no-img.jpg') }}"
                                                                            style="height: 350px;" width="270px">
                                                                    @endif

                                                                </a>
                                                                <div class="flash">
                                                                    <span class="onnew"><span
                                                                            class="text">New</span></span>
                                                                </div>
                                                                <div class="group-button">
                                                                    <div class="add-to-cart">
                                                                        <a href="{{ route('addcartshop', ['slug' => $item->slug]) }}"
                                                                            class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm
                                                                            vào giỏ hàng
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="product-info">
                                                                <h3 class="product-name product_title">
                                                                    <a href="{{ route('frontend.detail', ['slug' => $item->slug]) }}"
                                                                        tabindex="0">{{ $item->name }}</a>
                                                                </h3>
                                                                <span class="price"><span
                                                                        class="akasha-Price-amount amount"><span
                                                                            class="akasha-Price-currencySymbol"></span>Giá
                                                                        chỉ từ: {{ number_format($item->price) }}
                                                                        đ</span></span>
                                                                {{-- <div class="rating-wapper nostar">
                                    <div class="star-rating"><span style="width:0%">Rated <strong
                                            class="rating">0</strong> out of 5</span></div>
                                    <span class="review">(0)</span></div> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="section-012">
                                    <div class="container">
                                        <div class="row">
                                            <div
                                                class="col-xl-10 offset-xl-2 col-lg-8 offset-lg-4 col-md-9 offset-md-3 col-sm-12">
                                                <div class="akasha-products style-03">
                                                    <div class="response-product product-list-owl owl-slick equal-container better-height"
                                                        data-slick="{&quot;arrows&quot;:true,&quot;slidesMargin&quot;:30,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:2,&quot;rows&quot;:1}"
                                                        data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;30&quot;}}]">
                                                        @foreach ($product_new as $item)
                                                            <div class="product-item on_sale style-03 rows-space-0 post-36 product type-product status-publish has-post-thumbnail product_cat-table product_cat-bed product_tag-light product_tag-table product_tag-sock first instock sale shipping-taxable purchasable product-type-simple"
                                                                data-slick-index="0"
                                                                style="margin-right: 30px; width: 520px;"
                                                                aria-hidden="false" tabindex="0" role="tabpanel"
                                                                id="slick-slide10" aria-describedby="slick-slide-control10">
                                                                <div class="product-inner">
                                                                    <div class="product-thumb">
                                                                        <a class="thumb-link" href="#" tabindex="0">
                                                                            @if ($item->productdetail->count() > 0)
                                                                                <img class="img-responsive"
                                                                                    src="{{ asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                                                                    alt="Print In White" width="275"
                                                                                    style="height: 247px;">
                                                                            @else
                                                                                <img src="{{ asset('images/no-img.jpg') }}"
                                                                                    width="275" style="height: 247px;">
                                                                            @endif

                                                                        </a>
                                                                    </div>
                                                                    <div class="product-info equal-elem">
                                                                        <h3 class="product-name product_title">
                                                                            <a href="#"
                                                                                tabindex="-1">{{ $item->name }}</a>
                                                                        </h3>
                                                                        <span class="price">
                                                                            <span class="akasha-Price-amount amount">
                                                                                <span class="akasha-Price-currencySymbol">
                                                                                </span>
                                                                                Giá chỉ từ:
                                                                                {{ number_format($item->price) }} đ
                                                                            </span>

                                                                            <div class="add-to-cart"
                                                                                style="margin-top: 30px;">
                                                                                <a href="{{ route('addcartshop', ['slug' => $item->slug]) }}"
                                                                                    data-quantity="1"
                                                                                    class="button product_type_simple add_to_cart_button ajax_add_to_cart"
                                                                                    data-product_id="28"
                                                                                    data-product_sku="005K938"
                                                                                    aria-label="Add “Classic Shirt” to your cart"
                                                                                    rel="nofollow" tabindex="-1">Thêm vào
                                                                                    giỏ hàng</a>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="section-013">
                                    <div class="container">
                                        <div class="row">
                                            @foreach ($product_featured as $item)
                                                <div class="col-md-4">
                                                    <div class="akasha-banner style-03 left-center">
                                                        <div class="banner-inner">
                                                            <figure class="banner-thumb">
                                                                @if ($item->productdetail->count() > 0)
                                                                    <img src="{{ asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                                                        class="attachment-full size-full" alt="img"
                                                                        style="width: 370px;height: 472px;">
                                                                @else
                                                                    <img src="{{ asset('images/no-img.jpg') }}"
                                                                        class="attachment-full size-full" alt="img"
                                                                        style="width: 370px;height: 472px;">
                                                                @endif
                                                            </figure>
                                                            <div class="banner-info ">
                                                                <div class="banner-content">
                                                                    <div class="title-wrap">
                                                                        <h6 class="title">
                                                                            <a target="_self" href="#">Xem ngay</a>
                                                                        </h6>
                                                                    </div>
                                                                    <div class="cate">Xu hướng</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                <div class="section-001 section-005">
                                    <div class="container">
                                        <div class="row">
                                            @foreach ($categorie_take as $item)
                                                <div class="col-md-12 col-lg-6">
                                                    <div class="akasha-banner style-05 left-center">
                                                        <div class="banner-inner">
                                                            <figure class="banner-thumb">
                                                                <img src="{{ asset('images/category/') }}/{{ $item->image }}"
                                                                    class="attachment-full size-full" alt="img">
                                                            </figure>
                                                            <div class="banner-info ">
                                                                <div class="banner-content">
                                                                    <div class="title-wrap">
                                                                        <h6 class="title">Siêu ưu đãi</h6>
                                                                    </div>
                                                                    <div class="button-wrap">
                                                                        <div class="discount">
                                                                            15% <span>giảm giá</span>
                                                                        </div>
                                                                        <div class="subtitle">
                                                                            Hiện có sẵn trong kho của chúng tôi.
                                                                        </div>
                                                                        <a class="button" target="_self"
                                                                            href="{{ route('frontend.shop', ['cat' => $item->slug]) }}"><span>Xem
                                                                                ngay</span></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                <div class="section-007">
                                    <div class="container">
                                        <div class="row">
                                            @foreach ($category_active as $item)
                                                <div class="col-md-12 col-lg-4">
                                                    <div class="akasha-products style-06">
                                                        <h3 class="title">
                                                            <span>{{ $item->name }}</span>
                                                        </h3>
                                                        <div class="response-product product-list-owl owl-slick equal-container better-height"
                                                            data-slick="{&quot;arrows&quot;:true,&quot;slidesMargin&quot;:30,&quot;dots&quot;:false,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:1,&quot;rows&quot;:3}"
                                                            data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;10&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;20&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesMargin&quot;:&quot;30&quot;}}]">
                                                            @foreach ($item->products as $item)
                                                                <div
                                                                    class="product-item best-selling style-06 rows-space-30 post-25 product type-product status-publish has-post-thumbnail product_cat-light product_cat-chair product_cat-specials product_tag-light product_tag-sock first instock sale featured shipping-taxable purchasable product-type-simple">
                                                                    <div class="product-inner">
                                                                        <div class="product-thumb">
                                                                            <a class="thumb-link"
                                                                                href="{{ route('frontend.detail', ['slug' => $item->slug]) }}"
                                                                                tabindex="0">
                                                                                @if ($item->productdetail->count() > 0)
                                                                                    <img class="img-responsive"
                                                                                        src="{{ asset('images/product/') }}/{{ $item->productdetail[0]->image }}"
                                                                                        alt="" alt="Utility Pockets"
                                                                                        style="width: 88px;height: 112px;">
                                                                                @else
                                                                                    <img src="{{ asset('images/no-img.jpg') }}"
                                                                                        style="width: 88px;height: 112px;">
                                                                                @endif

                                                                            </a>
                                                                        </div>
                                                                        <div class="product-info">
                                                                            <h3 class="product-name product_title">
                                                                                <a href="{{ route('frontend.detail', ['slug' => $item->slug]) }}"
                                                                                    tabindex="0">{{ $item->name }}</a>
                                                                            </h3>
                                                                            <h3 class="product-name product_title">
                                                                                <a href="{{ route('frontend.detail', ['slug' => $item->slug]) }}"
                                                                                    tabindex="0">Thương hiệu:
                                                                                    {{ $item->trademark->name }}</a>
                                                                            </h3>
                                                                            <div class="rating-wapper ">
                                                                                <div class="star-rating">
                                                                                    <span style="width:100%">Rated
                                                                                        <strong
                                                                                            class="rating">5.00</strong>
                                                                                        out of 5
                                                                                    </span>
                                                                                </div>
                                                                                <span class="review">(1)</span>

                                                                            </div>
                                                                            <span class="price"><ins><span
                                                                                        class="akasha-Price-amount amount"><span
                                                                                            class="akasha-Price-currencySymbol"></span>Chỉ
                                                                                        từ:
                                                                                        {{ number_format($item->price) }}
                                                                                        đ</span></ins></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                <div class="section-014">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-3">
                                                <div class="akasha-iconbox style-02">
                                                    <div class="iconbox-inner">
                                                        <div class="icon">
                                                            <span class="flaticon-rocket-ship"></span>
                                                        </div>
                                                        <div class="content">
                                                            <h4 class="title">Vận chuyển toàn cầu</h4>
                                                            <div class="desc">Chúng tôi giao hàng đến hơn 200
                                                                quốc gia
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="akasha-iconbox style-02">
                                                    <div class="iconbox-inner">
                                                        <div class="icon">
                                                            <span class="flaticon-padlock"></span>
                                                        </div>
                                                        <div class="content">
                                                            <h4 class="title">Vận chuyển an toàn</h4>
                                                            <div class="desc">Thanh toán bằng các phương thức
                                                                thanh toán an toàn và phổ biến nhất trên thế giới.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="akasha-iconbox style-02">
                                                    <div class="iconbox-inner">
                                                        <div class="icon">
                                                            <span class="flaticon-recycle"></span>
                                                        </div>
                                                        <div class="content">
                                                            <h4 class="title">365 ngày hoàn trả</h4>
                                                            <div class="desc">Hỗ trợ 24/24 để có trải nghiệm mua
                                                                sắm suôn sẻ.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="akasha-iconbox style-02">
                                                    <div class="iconbox-inner">
                                                        <div class="icon">
                                                            <span class="flaticon-support"></span>
                                                        </div>
                                                        <div class="content">
                                                            <h4 class="title">Tự tin mua sắm</h4>
                                                            <div class="desc">Bảo vệ người mua của chúng tôi bao
                                                                gồm việc mua hàng của bạn từ khi nhấp chuột đến khi giao
                                                                hàng.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="section-001">
                                    <div class="container">
                                        <div class="akasha-slide">
                                            <div class="owl-slick equal-container better-height slick-initialized slick-slider"
                                                data-slick="{&quot;arrows&quot;:true,&quot;slidesMargin&quot;:60,&quot;dots&quot;:false,&quot;infinite&quot;:false,&quot;speed&quot;:300,&quot;slidesToShow&quot;:5,&quot;rows&quot;:1}"
                                                data-responsive="[{&quot;breakpoint&quot;:480,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesMargin&quot;:&quot;30&quot;}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesMargin&quot;:&quot;30&quot;}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesMargin&quot;:&quot;40&quot;}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesMargin&quot;:&quot;50&quot;}},{&quot;breakpoint&quot;:1500,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesMargin&quot;:&quot;60&quot;}}]">
                                                <div class="slick-list draggable">
                                                    <div class="slick-track"
                                                        style="opacity: 1; width: 1470px; transform: translate3d(0px, 0px, 0px);">
                                                        @foreach ($trademarks as $item)
                                                        <div class="dreaming_single_image dreaming_content_element az_align_center slick-slide slick-current slick-active first-slick"
                                                            data-slick-index="0" aria-hidden="false"
                                                            style="margin-right: 60px; width: 186px;" tabindex="0">
                                                            <figure class="dreaming_wrapper az_figure">
                                                                <div
                                                                    class="az_single_image-wrapper   az_box_border_grey effect bounce-in ">
                                                                   <a href="{{ route('frontend.trademark', ['slug' => $item->slug]) }}">
                                                                        <img src="{{ asset('images/trademark/') }}/{{ $item->image }}"
                                                                            class="az_single_image-img attachment-full"
                                                                            alt="img" width="200" height="100">
                                                                   </a>
                                                                </div>
                                                            </figure>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endsection
