@extends('backend.Master.layout')
@section('title')
	Lịch sử giá sản phẩm
@endsection
@section('main')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Lịch sử giá</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Lịch sử giá sản phẩm</li>
                        </ol>
                    </nav>
                </div>
                <div class="ps-9" style="margin-left: 1000px;">
                    <form class="float-lg-end" action="" method="get">
                        <div class="row row-cols-lg-auto g-2">
                            <div class="col-12">
                                <div class="position-relative">
                                    <input type="text" class="form-control ps-5" placeholder="Tìm sản phẩm..." name="search"> <span class="position-absolute top-50 product-show translate-middle-y"><i class="bx bx-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h6 class="mb-0 text-uppercase">Lịch sử giá sản phẩm</h6>
                    <hr/>
                    <div class="card">
                        <div class="card-body">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Sản phẩm</th>
                                        <th scope="col">Giá nhập</th>
                                        <th scope="col">Giá bán</th>
                                        <th scope="col">Thời gian cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($history as $key => $item)
                                    <tr>
                                        <th scope="row">{{ $history->firstitem() + $key }}</th>
                                        <td>{{$item->history->name}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>{{$item->import_price}}</td>
                                        <td>{{$item->CarbonLocaleupdated_at()}}</td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ $history->links() }}
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page wrapper -->
@endsection