@extends('backend.Master.layout')
@section('title')
	Sửa kho hàng
@endsection
@section('main')
<!--start page wrapper -->
<div class="page-wrapper">
    <div class="page-content">
         <!--breadcrumb-->
         <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Sửa kho hàng</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Sửa kho hàng sản phẩm</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h6 class="mb-0 text-uppercase">Sửa kho hàng sản phẩm</h6>
                <hr>
                <div class="card border-top border-0 border-4 border-white">
                    <div class="card-body">
                        <div class="border p-4 rounded">
                            @foreach ($errors->all() as $message)
                                <div class="alert border-0 alert-dismissible fade show">
                                    <div class="text-white">{{$message}}</div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endforeach
                            <form action="{{route('admin.warehouses.update',['id'=>$warehouse->id])}}" method="post">
                                @csrf
                                <div class="row mb-3">
                                    <label for="inputEnterYourName" class="col-sm-3 col-form-label">Tên kho hàng <span class="requiredLable">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" placeholder="Nhập tên kho hàng" value="{{$warehouse->ware_house_name}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputEnterYourName" class="col-sm-3 col-form-label">Vị trí <span class="requiredLable">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="location" placeholder="Nhập Vị trí" value="{{$warehouse->location}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-sm-3 col-form-label">Danh mục <span class="requiredLable">*</span></label>
                                    <div class="col-sm-9">
                                        <select id="people" class="form-select" name="cat_id[]" multiple>
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-light px-5">Sửa kho hàng</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('style')
<link href="{{asset('backend/assets/css/example-styles.css') }}" rel="stylesheet">

@endsection
@push('scripts')
<script src="{{asset('backend/index.js') }}"></script>
<script src="{{asset('backend/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{asset('backend/assets/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript">
$(function(){
    $('#people').multiSelect();
});
</script>
@endpush