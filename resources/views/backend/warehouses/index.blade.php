@extends('backend.Master.layout')
@section('title')
	Danh sách kho hàng
@endsection
@section('main')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Danh sách kho hàng</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Danh sách kho hàng</li>
                        </ol>
                    </nav>
                </div>
                <div class="ps-9" style="margin-left: 900px;">
                    <form class="float-lg-end" action="" method="get">
                        <div class="row row-cols-lg-auto g-2">
                            <div class="col-12">
                                <div class="position-relative">
                                    <input type="text" class="form-control ps-5" placeholder="Tìm..." name="search"> <span class="position-absolute top-50 product-show translate-middle-y"><i class="bx bx-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <a href="{{route('admin.warehouses.create')}}"><h6 class="mb-0 text-uppercase">Thêm kho hàng</h6></a>
                    <hr/>
                    <div class="card">
                        <div class="card-body">
                            {!! alert_success(session('thong-bao')) !!}
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Mã kho hàng</th>
                                        <th scope="col">Tên kho hàng</th>
                                        <th scope="col">Vị trí</th>
                                        <th scope="col">Xem kho hàng</th>
                                        <th scope="col">Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($wareHouses as $key => $item)
                                    <tr>
                                        <th scope="row">{{ $wareHouses->firstitem() + $key }}</th>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->ware_house_name}}</td>
                                        <td>{{$item->location}}</td>
                                        <td>
                                            <a class="btn btn-warning"
                                                href="{{route('admin.warehouses.detail',['id'=>$item->id])}}">Chi tiết</a>
                                        </td>
                                        <td>
                                            <div class="d-flex order-actions">
                                                <a href="{{ route('admin.warehouses.edit', ['id' => $item->id]) }}"><svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-edit text-white">
                                                        <path
                                                            d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7">
                                                        </path>
                                                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <a href="javascript:void(0)"
                                                    onclick="$('#dialog-example_{{ $item->id }}').modal('show');"
                                                    data-toggle="modal" data-target="#exampleModal_{{ $item->id }}">
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-trash-2 text-white">
                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                        <path
                                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                        </path>
                                                        <line x1="10" y1="11" x2="10" y2="17"></line>
                                                        <line x1="14" y1="11" x2="14" y2="17"></line>
                                                    </svg>
                                                </a>
                                            </div>
                                            
                                        </td>
                                        <div id="dialog-example_{{ $item->id }}" class="modal fade" id="exampleModal_{{ $item->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div id="dialog-example_{{ $item->id }}" class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bạn muốn xóa kho
                                                            {{ $item->ware_house_name }} ?</h5>
                                                        <i class="lni lni-close" data-dismiss="modal" aria-label="Close"
                                                            onclick="$('#dialog-example_{{ $item->id }}').modal('hide');"></i>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-dismiss="modal"
                                                            onclick="$('#dialog-example_{{ $item->id }}').modal('hide');">Hủy</button>
                                                        <a href="{{ route('admin.warehouses.remove', ['id' => $item->id, 'name' => $item->ware_house_name]) }}"><button
                                                                type="button" class="btn btn-danger">Xóa</button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ $wareHouses->links() }}
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page wrapper -->
@endsection