@extends('backend.Master.layout')
@section('title')
	Thêm nhà cung cấp
@endsection
@section('main')
<!--start page wrapper -->
<div class="page-wrapper">
    <div class="page-content">
         <!--breadcrumb-->
         <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Thêm nhà cung cấp</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Thêm nhà cung cấp sản phẩm</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h6 class="mb-0 text-uppercase">Thêm nhà cung cấp sản phẩm</h6>
                <hr>
                <div class="card border-top border-0 border-4 border-white">
                    <div class="card-body">
                        <div class="border p-4 rounded">
                            @foreach ($errors->all() as $message)
                                <div class="alert border-0 alert-dismissible fade show">
                                    <div class="text-white">{{$message}}</div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endforeach
                            <form action="{{route('admin.manufacture.store')}}" method="post">
                                @csrf
                                <div class="row mb-3">
                                    <label for="inputEnterYourName" class="col-sm-3 col-form-label">Tên nhà cung cấp</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" placeholder="Nhập tên nhà cung cấp">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Slug</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="slug" placeholder="Nhập slug nhà cung cấp">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-light px-5">Tạo mới</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection