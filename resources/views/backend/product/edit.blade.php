@extends('backend.Master.layout')

@section('title')
    Sửa sản phẩm {{ $product->name }}
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Sửa sản phẩm {{ $product->name }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-body p-4">
                    <div class="form-body mt-4">
                        <form action="{{route('admin.product.update',['id'=>$product->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-8">
                                    @foreach ($errors->all() as $message)
                                        <div class="alert border-0 border-start border-5 border-white alert-dismissible fade show py-2">
                                            <div class="d-flex align-items-center">
                                                <div class="font-35 text-white"><i class="bx bx-info-circle"></i>
                                                </div>
                                                <div class="ms-3">
                                                    <h6 class="mb-0 text-white">Cảnh báo!</h6>
                                                    <div>{{ $message }}</div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    @endforeach
                                    <form action="" method="post"></form>
                                    <div class="border border-3 p-4 rounded">
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Tên sản phẩm <span class="requiredLable">*</span></label>
                                            <input type="text" name="name" class="form-control" id="inputProductTitle"
                                                placeholder="Nhập tên" value="{{ $product->name }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Title sản phẩm <span class="requiredLable">*</span></label>
                                            <input type="text" name="title" class="form-control" id="inputProductTitle"
                                                placeholder="Nhập titile sản phẩm" value="{{ $product->title }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Trạng thái <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="state">
                                                <option @if ($product->state == 1) selected @endif value="1">Hiện thị</option>
                                                <option @if ($product->state == 0) selected @endif value="0">Ẩn</option>
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Nổi bật <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="featured">
                                                <option @if ($product->featured == 1) selected @endif value="1">Nổi bật</option>
                                                <option @if ($product->featured == 0) selected @endif value="0">Không</option>
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Danh mục <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="cat_id">
                                                @foreach ($category as $item)
                                                    <option @if ($product->cat_id == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputPhoneNo2" class="form-label">Thương hiệu <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="trademark_id">
                                                @foreach ($trademarks as $item)
                                                    <option  @if ($product->trademark_id == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Giá bán <span class="requiredLable">*</span></label>
                                            <input type="number" class="form-control" name="price" value="{{ $product->price }}"
                                                    placeholder="Nhập giá nhập sản phẩm">
                                        </div>
                                        <div class="mb-3">
                                            <label>Kiểu dáng <span class="requiredLable">*</span></label>
                                            <input type="text" name="style" class="form-control"
                                                value="{{ $product->style }}">
                                        </div>
                                        <div class="mb-3">
                                            <label>Chất liệu <span class="requiredLable">*</span></label>
                                            <input type="text" name="material" class="form-control"
                                                value="{{ $product->material }}">
                                        </div>
                                        <div class="mb-3">
                                            <label>Màu sắc <span class="requiredLable">*</span></label>
                                            <input type="text" name="color" class="form-control"
                                                value="{{ $product->color }}">
                                        </div>
                                        <div class="mb-3">
                                            <label>Kích cỡ <span class="requiredLable">*</span></label>
                                            <input type="text" name="size" class="form-control"
                                                value="{{ $product->size }}">
                                        </div>
                                        <div class="mb-3">
                                            <label>Xuất xứ <span class="requiredLable">*</span></label>
                                            <input type="text" name="origin" class="form-control"
                                                value="{{ $product->origin }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Nhà cung cấp <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="producer_id">
                                                @foreach ($producers as $item)
                                                    <option  @if ($product->producer_id == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Nhà sản xuất <span class="requiredLable">*</span></label>
                                            <select class="form-select" name="manufacture_id">
                                                @foreach ($manufactures as $item)
                                                    <option  @if ($product->manufacture_id == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="mb-3">
                                            <label for="inputVendor" class="form-label">Nhà sản xuất</label>
                                            <select class="form-select" id="inputVendor" name="trademark_id">
                                                <select class="form-select" name="manufacture_id">
                                                    @foreach ($manufactures as $item)
                                                        <option @if ($product->manufacture_id == $item->id) selected @endif  value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </select>
                                        </div> --}}
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label ckeditor">Giới thiệu <span class="requiredLable">*</span></label>
                                            <textarea class="form-control ckeditor" id="description"
                                                rows="3" name="describer">{!!$product->info!!}</textarea>
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label ckeditor">Description <span class="requiredLable">*</span></label>
                                            <textarea class="form-control ckeditor" id="description"
                                                rows="3" name="describer">{!!$product->describer!!}</textarea>
                                        </div>
                                        @if ($img_detail->count()>0)
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Ảnh chi tiết</label>
                                        </div>
                                        @endif
                                        @foreach ($img_detail as $image)
                                            <div class="mb-3">
                                                <img id="" name="{{ $image->id }}" class="thumbnail" width="10%" height="10%"
                                                    src="{{asset('images/product/') }}/{{ $image->image }}">
                                                <a href="javascript:void(0)" onclick="$('#dialog-example_{{ $image->id }}').modal('show');"
                                                    data-toggle="modal" data-target="#exampleModal">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                        stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-edit text-white">
                                                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7">
                                                        </path>
                                                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z">
                                                        </path>
                                                    </svg>
                                                </a>
                                                <a onclick="$('#dialog-example_2{{ $image->id }}').modal('show');"
                                                    data-toggle="modal" data-target="#exampleModal2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                        stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-trash-2 text-white">
                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                        <path
                                                            d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                        </path>
                                                        <line x1="10" y1="11" x2="10" y2="17"></line>
                                                        <line x1="14" y1="11" x2="14" y2="17"></line>
                                                    </svg>
                                                </a>
                                                <!-- Modal Edit -->
                                                <div id="dialog-example_{{ $image->id }}" class="modal fade"
                                                    id="exampleModal" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <form action="{{route('admin_prd_editimagedetailpost',['id'=>$image->id])}}" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                            <div id="dialog-example_{{ $image->id }}" class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Hãy chọn ảnh
                                                                        muốn thay đổi</h5>
                                                                    <i class="lni lni-close" data-dismiss="modal"
                                                                        aria-label="Close"
                                                                        onclick="$('#dialog-example_{{ $image->id }}').modal('hide');"></i>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <input type="hidden" name="prd_id" value="{{ $product->id }}">
                                                                    <input type="hidden" name="imageDetail_id" value="{{ $image->id }}">
                                                                    <input type="hidden" name="imageDetail_name" value="{{ $image->image }}">
                                                                    <input type="file" name="{{ $image->id }}">
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-danger">Thay thế ảnh cũ</button>
                                                                    <button type="button" class="btn btn-success"
                                                                        data-dismiss="modal"
                                                                        onclick="$('#dialog-example_{{ $image->id }}').modal('hide');">Hủy</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <!-- Modal Delete-->
                                                <div id="dialog-example_2{{ $image->id }}" class="modal fade"
                                                    id="exampleModal2" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                            <div id="dialog-example_2{{ $image->id }}" class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Hãy chọn xóa, nếu bạn muốn xóa ảnh này!</h5>
                                                                    <i class="lni lni-close" data-dismiss="modal"
                                                                        aria-label="Close"
                                                                        onclick="$('#dialog-example_2{{ $image->id }}').modal('hide');"></i>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <a href="{{route('admin_prd_delete_imagedetail',['id'=>$image->id])}}"><button type="button" class="btn btn-danger">Tôi muốn xóa</button></a>
                                                                    <button type="button" class="btn btn-success"
                                                                        data-dismiss="modal"
                                                                        onclick="$('#dialog-example_2{{ $image->id }}').modal('hide');">Hủy</button>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Thêm ảnh chi tiết</label>
                                        </div>
                                        <div class="mb-3">
                                            <input id="image-uploadify" type="file" name="images[]" multiple>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="border border-3 p-4 rounded">
                                        <div class="row g-3">
                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <input type="hidden" value="{{$product->cat_id}}" name="cat_id">
                                                    <button type="submit" class="btn btn-light">Lưu lại</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
    @push('scripts')
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
    @endpush
