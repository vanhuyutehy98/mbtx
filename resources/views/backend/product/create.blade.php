@extends('backend.Master.layout')
@section('title')
    Tạo sản phẩm
@endsection
@section('main')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Tạo sản phẩm</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Tạo sản phẩm sản phẩm</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h6 class="mb-0 text-uppercase">Tạo sản phẩm sản phẩm</h6>
                    <hr>
                    <div class="card border-top border-0 border-4 border-white">
                        <div class="card-body">
                            <div class="border p-4 rounded">
                                @foreach ($errors->all() as $message)
                                    <div class="alert border-0 border-start border-5 border-white alert-dismissible fade show py-2">
                                        <div class="d-flex align-items-center">
                                            <div class="font-35 text-white"><i class="bx bx-info-circle"></i>
                                            </div>
                                            <div class="ms-3">
                                                <h6 class="mb-0 text-white">Cảnh báo!</h6>
                                                <div>{{ $message }}</div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                @endforeach
                                <form action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Tên sản phẩm <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name"
                                                placeholder="Nhập tên sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Slug</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="slug"
                                                placeholder="Nhập slug sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="title"
                                                placeholder="Nhập title">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Trạng thái <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <select class="form-select" name="state">
                                                <option value="0">Ẩn</option>
                                                <option value="1">Hiện thị</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Nổi bật <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <select class="form-select" name="featured">
                                                <option value="0">Không</option>
                                                <option value="1">Có</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Danh mục <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <select class="form-select" name="cat_id">
                                                @foreach ($category as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Thương hiệu <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <select class="form-select" name="trademark_id">
                                                @foreach ($trademarks as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Giá bán <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="price"
                                                placeholder="Nhập giá bán sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Kiểu dáng <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="style"
                                                placeholder="Nhập kiểu dáng sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Chất liệu <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="material"
                                                placeholder="Nhập chất liệu sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Màu sắc <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="color"
                                                placeholder="Nhập màu sắc sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Kích cỡ <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="size"
                                                placeholder="Nhập kích cỡ sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Xuất xứ <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="origin"
                                                placeholder="Nhập xuất xứ sản phẩm">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Nhà cung cấp <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="custom-select">
                                                <select class="form-select" name="producer_id">
                                                    @foreach ($producers as $item)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Nhà sản xuất <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="custom-select">
                                                <select class="form-select" name="manufacture_id">
                                                    @foreach ($manufactures as $item)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Giới thiệu <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            {{-- <textarea class="form-control" name="info" id="" cols="30" rows="10"></textarea> --}}
                                            <textarea class="form-control ckeditor" id="description" rows="3"
                                            name="info">{{ old('info') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="inputEnterYourName" class="col-sm-3 col-form-label">Chi tiết <span class="requiredLable">*</span></label>
                                        <div class="col-sm-9">
                                            {{-- <textarea class="form-control" name="describer" id="" cols="30" rows="10"></textarea> --}}
                                            <textarea class="form-control ckeditor" id="description" rows="3"
                                            name="describer">{{ old('describer') }}</textarea>
                                        </div>
                                    </div>
                                    {{-- <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Ảnh sản phẩm</label>
                                        <div class="col-sm-9">
                                            <input id="browse" type="file" onchange="previewFiles()" multiple>
                                            <div id="preview"></div>
                                        </div>
                                    </div> --}}
                                    <div class="row mb-3">
                                        <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Ảnh sản phẩm</label>
                                        <div class="col-sm-9">
                                            <input id="browse" type="file" name="images[]" onchange="previewFiles()" multiple>
                                            <div id="preview"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 col-form-label"></label>
                                        <div class="col-sm-9">
                                            <button type="submit" class="btn btn-light px-5">Tạo mới</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('style')
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link type="text/css" rel="stylesheet" href="http://example.com/image-uploader.min.css">
@endsection
@push('scripts')
    <script>
        
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);

        function previewFiles() {

            var preview = document.querySelector('#preview');
            var files = document.querySelector('input[type=file]').files;

            function readAndPreview(file) {

                // Make sure `file.name` matches our extensions criteria
                if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
                    var reader = new FileReader();

                    reader.addEventListener("load", function() {
                        var image = new Image();
                        image.height = 100;
                        image.title = file.name;
                        image.src = this.result;
                        preview.appendChild(image);
                    }, false);

                    reader.readAsDataURL(file);
                }

            }

            if (files) {
                [].forEach.call(files, readAndPreview);
            }

        }
    </script>
    <script type="text/javascript" src="http://example.com/jquery.min.js"></script>
    <script type="text/javascript" src="http://example.com/image-uploader.min.js"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
@endpush
