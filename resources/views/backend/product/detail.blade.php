@extends('backend.Master.layout')
@section('title')
    Chi tiết sản phẩm {{ $product->name }}
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Chi tiết sản phẩm {{ $product->name }}
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="row g-0">
                    <div class="col-md-4 border-end">
                        @if ($product->productdetail->count() > 0)
                            <img src="{{asset('images/product/') }}/{{ $product->productdetail[0]->image }}" class="img-fluid" alt="{{ $product->name }}">
                        @else
                            <img src="{{asset('images/no-img.jpg') }}" alt="{{ $product->name }}" class="img-fluid" style="width: 100%;">
                        @endif
                        
                        <div class="row mb-3 row-cols-auto g-2 justify-content-center mt-3">
                            <div class="col"><img src="{{asset('images/product/') }}/{{ $product->image }}"
                                width="70" class="border rounded cursor-pointer"
                                alt="{{ $product->image }}"></div>
                        <div class="col"><img src="{{asset('images/product/') }}/{{ $product->image2 }}"
                                width="70" class="border rounded cursor-pointer" alt="{{ $product->image2 }}">
                        </div>
                            @foreach ($product->productdetail as $prd_detail_item)
                                <div class="col"><img src="{{asset('images/product/') }}//{{ $prd_detail_item->image }}"
                                        width="70" class="border rounded cursor-pointer"
                                        alt="{{ $prd_detail_item->image }}"></div>

                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h4 class="card-title">Tên sản phẩm: {{ $product->name }}</h4>
                            <div class="mb-3">
                                <span class="price h4">Giá: {{ number_format($product->price, 0, '', ',') }} đ</span>
                            </div>
                            <div class="mb-3">
                                <p>Giới thiệu:</p>
                                <p class="card-text fs-6">{!! $product->info !!}</p>
                            </div>
                            <dl class="row">
                                <p>Kiểu dáng: {{$product->style}}</p>
                                <p>Chất liệu: {{$product->material}}</p>
                                <p>Màu sắc: {{$product->color}}</p>
                                <p>Kích cỡ: {{$product->size}}</p>
                                <p>Xuất xứ: {{$product->origin}}</p>
                                <p>Nhà cung cấp: {{$product->Producer->name}}</p>
                                <p>Nhà sản xuất: {{$product->Manufacture->name}}</p>
                            </dl>
                            <hr>
                            <div class="d-flex gap-3 py-3">
                                <div>{{$product->comments->count()}} bình luận</div>
                            </div>
                            <div class="row row-cols-auto row-cols-1 row-cols-md-3 align-items-center">
                                {{-- <div class="col">
                                    <label class="form-label">Số lượng</label>
                                    <div class="input-group input-spinner">
                                        <input disabled type="text" class="form-control" value="{{ $product->amount }}">
                                    </div>
                                </div> --}}
                                <div class="col">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="card-body">
                    <ul class="nav nav-tabs mb-0" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" data-bs-toggle="tab" href="#primaryhome" role="tab"
                                aria-selected="true">
                                <div class="d-flex align-items-center">
                                    <div class="tab-icon"><i class='bx bx-comment-detail font-18 me-1'></i>
                                    </div>
                                    <div class="tab-title">Mô tả</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" data-bs-toggle="tab" href="#primarycontact" role="tab"
                                aria-selected="false">
                                <div class="d-flex align-items-center">
                                    <div class="tab-icon"><i class='bx bx-star font-18 me-1'></i>
                                    </div>
                                    <div class="tab-title">Bình luận</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content pt-3">
                        <div class="tab-pane fade show active" id="primaryhome" role="tabpanel">
                            {!!$product->describer!!}
                        </div>
                        <div class="tab-pane fade" id="primarycontact" role="tabpanel">
                            @if ($product->comments->count()==0)
                            <p>Chưa có đánh giá sản phẩm</p>
                            @else
                            @foreach ($product->comments as $item)
                            <div class="mb-3">
                                <label for="inputProductTitle" class="form-label">{{ $item->users->name }}</label>
                                <input type="text" name="" class="form-control" value="{{ $item->comment }}" disabled>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
