@extends('backend.Master.layout')
@section('title')
	Thương hiệu
@endsection
@section('main')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Thương hiệu</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Thương hiệu sản phẩm</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-9 mx-auto">
                   <a href="{{route('admin.trademark.create')}}"> <h6 class="mb-0 text-uppercase">Thêm thương hiệu sản phẩm</h6></a>
                    <hr/>
                    <div class="card">
                        <div class="card-body">
                            {!! alert_success(session('thong-bao')) !!}
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col">Tên</th>
                                        <th scope="col">Hình ảnh</th>
                                        <th scope="col">Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($trademark as $key => $category_item)
                                        <tr>
                                            <th scope="row">{{ $trademark->firstitem() + $key }}</th>
                                            <td>{{ $category_item->name }}</td>
                                            <td><img class="" src="{{asset('images/trademark/') }}/{{$category_item->image}}" alt="" width="100px" height="60px"></td>
                                            <td>
                                                <a 
                                                    href="{{ route('admin.trademark.edit', ['slug' => $category_item->slug]) }}" 
                                                    class="btn btn-light">
                                                    <i class="bx bx-refresh me-0"></i>
                                                </a>
                                                <a 
                                                    onclick="$('#dialog-example_{{ $category_item->id }}').modal('show');" 
                                                    data-toggle="modal" 
                                                    data-target="#exampleModal" 
                                                    class="btn btn-light">
                                                    <i class="bx bx-trash-alt me-0"></i>
                                                </a>
                                                <div 
                                                    id="dialog-example_{{ $category_item->id }}" 
                                                    class="modal fade" 
                                                    id="exampleModal" 
                                                    tabindex="-1"
                                                    role="dialog" 
                                                    aria-labelledby="exampleModalLabel" 
                                                    aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div id="dialog-example_{{ $category_item->id }}" class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Bạn muốn xóa sản phẩm {{ $category_item->name }} ?</h5>
                                                                <i
                                                                    class="lni lni-close" 
                                                                    data-dismiss="modal" 
                                                                    aria-label="Close"
                                                                    onclick="$('#dialog-example_{{ $category_item->id }}').modal('hide');">
                                                                </i>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button 
                                                                    type="button" 
                                                                    class="btn btn-success" 
                                                                    data-dismiss="modal"
                                                                    onclick="$('#dialog-example_{{ $category_item->id }}').modal('hide');">Hủy
                                                                </button>
                                                                <a href="{{ route('admin.trademark.remove', ['id' => $category_item->id]) }}">
                                                                    <button type="button" class="btn btn-danger">Xóa</button>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ $trademark->links() }}
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page wrapper -->
@endsection