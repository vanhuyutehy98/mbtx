@extends('backend.Master.layout')
@section('title')
	Admin
@endsection
@section('style')
<link href="{{asset('backend/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
@endsection
@section('main')
	<div class="page-wrapper">
		<div class="page-content">
			<div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
				<div class="col">
					<div class="card radius-10 bg-primary bg-gradient">
						<div class="card-body">
							<div class="d-flex align-items-center">
								<div>
									<p class="mb-0 text-white">Hóa đơn chưa xử lý</p>
									<h4 class="my-1 text-white">{{$total_order}}</h4>
								</div>
								<div class="text-white ms-auto font-35"><i class="bx bx-cart-alt"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card radius-10 bg-danger bg-gradient">
						<div class="card-body">
							<div class="d-flex align-items-center">
								<div>
									<p class="mb-0 text-white">Doanh thu theo ngày</p>
									<h4 class="my-1 text-white">{{number_format($total_many_day)}}</h4>
								</div>
								<div class="text-white ms-auto font-35"><i class="bx bx-dollar"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card radius-10 bg-warning bg-gradient">
						<div class="card-body">
							<div class="d-flex align-items-center">
								<div>
									<p class="mb-0 text-dark">Bình luận bài viết</p>
									<h4 class="text-dark my-1">{{$total_cmt_blog}}</h4>
								</div>
								<div class="text-dark ms-auto font-35"><i class="bx bx-comment-detail"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card radius-10 bg-success bg-gradient">
						<div class="card-body">
							<div class="d-flex align-items-center">
								<div>
									<p class="mb-0 text-white">Bình luận sản phẩm</p>
									<h4 class="my-1 text-white">{{$total_cmt_product}}</h4>
								</div>
								<div class="text-white ms-auto font-35"><i class="bx bx-comment-detail"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-6 col-xl-4 d-flex">
					<div class="card radius-10 overflow-hidden w-100">
						<div class="card-body">
							<p>Tổng thu nhập theo tuần</p>
							<h4 class="mb-0">{{number_format($total_Week)}} VNĐ</h4>
							<hr>
							<p>Tổng thu nhập tháng {{$currentMonth}}</p>
							<h4 class="mb-0">{{number_format($total_Month)}} VNĐ</h4>
							<hr>
							<p>Tổng doanh thu theo năm {{$currentYear}}</p>
							<h4 class="mb-0">{{number_format($total_Year)}} VNĐ</h4>
							<div class="mt-5">
								<div class="chart-container-4">
									<canvas id="chart5"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="col-12 col-lg-6 col-xl-8 d-flex">
					<div class="card radius-10 w-100">
						<div class="card-header border-bottom">
							<div class="d-flex align-items-center">
								<div>
									<h6 class="mb-0">Đánh giá của khách hàng</h6>
								</div>
								<div class="font-22 ms-auto text-white"><i class="bx bx-dots-horizontal-rounded"></i>
								</div>
							</div>
						</div>
						<ul class="list-group list-group-flush">
							@foreach ($cmt_product as $item)
							<li class="list-group-item bg-transparent">
								<div class="d-flex align-items-center">
									<img src="../users/{{$item->users->image}}" alt="user avatar" class="rounded-circle"
										width="55" height="55">
									<div class="ms-3">
										<h6 class="mb-0">{{$item->product->name}}<small class="ms-4">{{ date('d-m-Y', strtotime($item->created_at)) }}</small></h6>
										<p class="mb-0 small-font">{{$item->users->name}} : {{$item->comment}}
										</p>
									</div>
								</div>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
			<div class="card radius-10">
				<div class="card-body">
					<div class="d-flex align-items-center">
						<div>
							<h5 class="mb-0">Đơn hàng chưa hoàn thành</h5>
						</div>
						<div class="font-22 ms-auto"><i class="bx bx-dots-horizontal-rounded"></i>
						</div>
					</div>
					<hr>
					<div class="table-responsive">
						<table class="table align-middle mb-0">
							<thead class="table-light">
								<tr>
									<th>STT</th>
									<th>Khách hàng</th>
									<th>Tổng tiền</th>
									<th>Ngày</th>
									<th>Tình trạng</th>
									<th>Hành động</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($orders as $key => $item)
									<tr>
										<td>#{{$key+1 }}</td>
										<td>
											{{ $item->name }}
										</td>
										<td>{{ number_format($item->total) }} VNĐ</td>
										<td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
										<td>
											<span style="background: rgb(214, 12, 29)" class="badge badge-danger">Chưa xử lý</span>
										</td>
										<td>
											<div class="d-flex order-actions">
												<a href="{{ route('admin.order.detail', ['id' => $item->id, 'name' => $item->slug]) }}"
													class=""><i class='bx bxs-edit'></i></a>
											</div>
										</td>
									</tr>
								@endforeach
			
							</tbody>
						</table>
			
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('scripts')
	<script src="{{asset('backend/assets/plugins/chartjs/js/Chart.min.js') }}"></script>
    <script src="{{asset('backend/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{asset('backend/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{asset('backend/assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{asset('backend/assets/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
    <script src="{{asset('backend/assets/plugins/jquery-knob/excanvas.js') }}"></script>
    <script src="{{asset('backend/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
    <script>
        $(function() {
            $(".knob").knob();
        });
    </script>
    <script src="{{asset('backend/assets/js/index.js') }}"></script>
@endpush