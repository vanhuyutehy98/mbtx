@extends('backend.Master.layout')

@section('title')
    Chi tiết quản trị {{ $user->name }}
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $user->name }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="container">
                <div class="main-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-column align-items-center text-center">
                                        <img src="{{asset('images/imageAdmin/') }}/{{ $user->image }}" alt="Admin"
                                            class="rounded-circle p-1 bg-primary" width="110">
                                        <div class="mt-3">
                                            <h4>{{ $user->name }}</h4>
                                            <p class="mb-1">{{ $user->phone }}</p>
                                            <p class="font-size-sm">{{ $user->address }}</p>
                                        </div>
                                    </div>
                                    <hr class="my-4" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body">
                                    <form action="{{ route('admin_user_editDetailpost', ['id' => $user->id]) }}" method="post">
                                        @csrf
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Tên</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                <input disabled type="text" class="form-control" value="{{ $user->name }}"
                                                    name="name" />
                                                {!! show_errors_request($errors, 'name') !!}
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                <input disabled type="text" class="form-control" value="{{ $user->email }}"
                                                    name="email" />
                                                {!! show_errors_request($errors, 'email') !!}
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Ngày sinh</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                <input disabled type="date" class="form-control"
                                                    value="{{ date('Y-m-d', strtotime($user->datetime)) }}"
                                                    name="datetime" />
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Điện thoại</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                <input disabled type="number" class="form-control" value="{{ $user->phone }}"
                                                    name="phone" />
                                                {!! alert_warning(session('thong-bao-loi')) !!}
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Địa chỉ</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                <input disabled type="text" class="form-control" value="{{ $user->address }}"
                                                    name="address" />
                                                {!! show_errors_request($errors, 'address') !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                {{-- <button type="submit" class="btn btn-light px-4">Lưu lại</button> --}}
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label for="inputProductDescription" class="form-label">Phân quyền quản trị</label>
                            <!-- Table  -->
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Xem</th>
                                        <th>Thêm</th>
                                        <th>Sửa</th>
                                        <th>Xóa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_manufacture"
                                                {!!show_ruler($userItem,'view_manufacture')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem nhà sản xuất</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_manufacture"
                                                {!!show_ruler($userItem,'add_manufacture')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm nhà sản xuất</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_manufacture"
                                                {!!show_ruler($userItem,'edit_manufacture')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa nhà sản xuất</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_manufacture"
                                                {!!show_ruler($userItem,'delete_manufacture')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa nhà sản xuất</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_producer"
                                                {!!show_ruler($userItem,'view_producer')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem nhà cung cấp</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_producer"
                                                {!!show_ruler($userItem,'add_producer')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm nhà cung cấp</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_producer"
                                                {!!show_ruler($userItem,'edit_producer')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa nhà cung cấp</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_producer"
                                                {!!show_ruler($userItem,'delete_producer')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa nhà cung cấp</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_orderImport"
                                                {!!show_ruler($userItem,'view_orderImport')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem hóa đơn nhập</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_orderImport"
                                                {!!show_ruler($userItem,'add_orderImport')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm hóa đơn nhập</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_orderImport"
                                                {!!show_ruler($userItem,'edit_orderImport')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa hóa đơn nhập</label>
                                            </div>
                                            </td>
                                            <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_orderImport"
                                                {!!show_ruler($userItem,'delete_orderImport')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa hóa đơn nhập</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_warehouse"
                                            {!!show_ruler($userItem,'view_warehouse')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Xem kho hàng</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_warehouse"
                                            {!!show_ruler($userItem,'add_warehouse')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Thêm kho hàng</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_warehouse"
                                            {!!show_ruler($userItem,'edit_warehouse')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Sửa kho hàng</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_warehouse"
                                            {!!show_ruler($userItem,'delete_warehouse')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Xóa kho hàng</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_product"
                                            {!!show_ruler($userItem,'view_product')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Xem bài viết</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_product"
                                            {!!show_ruler($userItem,'add_product')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Thêm sản phẩm</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_product"
                                            {!!show_ruler($userItem,'edit_product')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Sửa sản phẩm</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_product"
                                            {!!show_ruler($userItem,'delete_product')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck2">Xóa sản phẩm</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="view_user"
                                            {!!show_ruler($userItem,'view_user')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck3">Xem danh sách quản trị</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="add_user"
                                            {!!show_ruler($userItem,'add_user')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck3">Thêm quản trị</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="edit_user"
                                            {!!show_ruler($userItem,'edit_user')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck3">Sửa quản trị</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="delete_user"
                                            {!!show_ruler($userItem,'delete_user')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck3">Xóa quản trị</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_category"
                                            {!!show_ruler($userItem,'view_category')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem danh mục</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_category"
                                            {!!show_ruler($userItem,'add_category')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Thêm danh mục</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_category"
                                            {!!show_ruler($userItem,'edit_category')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Sửa danh mục</label>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_category"
                                            {!!show_ruler($userItem,'delete_category')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa danh mục</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_order"
                                            {!!show_ruler($userItem,'view_order')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem hóa đơn</label>
                                        </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_order"
                                                {!!show_ruler($userItem,'edit_order')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Xử lý</label>
                                            </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_order"
                                            {!!show_ruler($userItem,'delete_order')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa hóa đơn</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_comment_product"
                                            {!!show_ruler($userItem,'view_comment_product')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem bình luận sản phẩm</label>
                                        </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_comment_product"
                                                {!!show_ruler($userItem,'add_comment_product')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bình luận sản phẩm</label>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_comment_product"
                                            {!!show_ruler($userItem,'delete_comment_product')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa bình luận sản phẩm</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_comment_blog"
                                            {!!show_ruler($userItem,'view_comment_blog')!!}
                                            >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem bình luận bài viết</label>
                                        </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_comment_blog"
                                                {!!show_ruler($userItem,'view_comment_blog')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bình luận bài viết</label>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_comment_blog"
                                            {!!show_ruler($userItem,'delete_comment_blog')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa bình luận bài viết</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_blog"
                                            {!!show_ruler($userItem,'view_blog')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem bài viết</label>
                                        </div>
                                        </td>
                                        <td>
                                            {{-- <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_blog"
                                                {!!show_ruler($userItem,'add_blog')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bài viết</label>
                                            </div> --}}
                                        </td>
                                        <td>
                                            {{-- <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_blog"
                                                {!!show_ruler($userItem,'edit_blog')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bài viết</label>
                                            </div> --}}
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_blog"
                                            {!!show_ruler($userItem,'delete_blog')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa bài viết</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_trademark"
                                            {!!show_ruler($userItem,'view_trademark')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem thương hiệu</label>
                                        </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_trademark"
                                                {!!show_ruler($userItem,'add_trademark')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Thêm thương hiệu</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_trademark"
                                                {!!show_ruler($userItem,'edit_trademark')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa thương hiệu</label>
                                            </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_trademark"
                                            {!!show_ruler($userItem,'delete_trademark')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa thương hiệu</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_logo"
                                            {!!show_ruler($userItem,'view_logo')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem Logo</label>
                                        </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_logo"
                                                {!!show_ruler($userItem,'edit_logo')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa Logo</label>
                                            </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">

                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_slide"
                                            {!!show_ruler($userItem,'view_slide')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem Slide</label>
                                        </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_slide"
                                                {!!show_ruler($userItem,'add_slide')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Thêm Slide</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_slide"
                                                {!!show_ruler($userItem,'edit_slide')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa Slide</label>
                                            </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_slide"
                                            {!!show_ruler($userItem,'delete_slide')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa Slide</label>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="custom-control custom-checkbox">
                                            <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_contact"
                                            {!!show_ruler($userItem,'view_contact')!!}
                                                >
                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem trang liên hệ Website</label>
                                        </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input disabled type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_contact"
                                                {!!show_ruler($userItem,'edit_contact')!!}
                                                >
                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa trang liên hệ Website</label>
                                            </div>
                                        </td>
                                        <td>
                                        <div class="custom-control custom-checkbox">

                                        </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            <!-- Table  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
