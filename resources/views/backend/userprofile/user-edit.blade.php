@extends('backend.Master.layout')

@section('title')
    Sửa quản trị Website {{ $userItem->name }}
@endsection
@section('main')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Admin/Sửa quản trị
                                {{ $userItem->name }}
                            </li>

                        </ol>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="card-body p-4">
                    <div class="form-body mt-4">
                        <div class="row">
                            <form action="{{ route('admin_user_edit_post', ['id' => $userItem->id]) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="col-lg-12">
                                    @foreach ($errors->all() as $message)
                                        <div class="alert border-0 alert-dismissible fade show">
                                            <div class="text-white">{{$message}}</div>
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    @endforeach
                                    <div class="border border-3 p-4 rounded">
                                        <div class="mb-3">
                                            <label for="inputVendor" class="form-label">Tên <span class="requiredLable">*</span></label>
                                            <input type="text" name="name" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{ $userItem->name }}">
                                        </div>

                                        <div class="mb-3">
                                            <label for="inputVendor" class="form-label">Điện thoại <span class="requiredLable">*</span></label>
                                            <input type="text" name="phone" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{ $userItem->phone }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputVendor" class="form-label">Địa chỉ <span class="requiredLable">*</span></label>
                                            <input type="text" name="address" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{ $userItem->address }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputVendor" class="form-label">Ngày sinh <span class="requiredLable">*</span></label>
                                            <input type="date" name="datetime" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc"
                                                value="{{ date('Y-m-d', strtotime($userItem->datetime)) }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductTitle" class="form-label">Email <span class="requiredLable">*</span></label>
                                            <input type="text" name="email" class="form-control" id="inputProductTitle"
                                                placeholder="Bắt buộc" value="{{ $userItem->email }}">
                                        </div>
                                        <div class="mb-3">
                                            <label for="inputProductDescription" class="form-label">Ảnh</label>
                                            <input id="img" type="file" name="image" class="form-control hidden"
                                                onchange="changeImg(this)">
                                            <img id="avatar" name="image" class="thumbnail" width="100px" height="100px" style="    margin-top: 20px;"
                                                src="{{asset('images/imageAdmin/') }}/{{ $userItem->image }}">
                                        </div>
                                        <div class="mb-3">
                                            {{-- <input type="checkbox" name="add"
                                                {{ $userItem->hasPermissionTo('add') ? 'checked' : '' }}><label>
                                                Sửa</label><br />
                                            <input type="checkbox" name="edit"
                                                {{ $userItem->hasPermissionTo('edit') ? 'checked' : '' }}><label>
                                                Sửa</label><br />
                                            <input type="checkbox" name="delete"
                                                {{ $userItem->hasPermissionTo('delete') ? 'checked' : '' }}><label>
                                                Xóa</label><br /> --}}
                                            {{-- @role('super-admin', 'web')
                                            1
                                            @else
                                            2
                                            @endrole --}}
                                            <label for="inputProductDescription" class="form-label">Phân quyền quản trị</label>
                                            <!-- Table  -->
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Xem</th>
                                                        <th>Thêm</th>
                                                        <th>Sửa</th>
                                                        <th>Xóa</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_manufacture"
                                                                {!!show_ruler($userItem,'view_manufacture')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem nhà sản xuất</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_manufacture"
                                                                {!!show_ruler($userItem,'add_manufacture')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm nhà sản xuất</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_manufacture"
                                                                {!!show_ruler($userItem,'edit_manufacture')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa nhà sản xuất</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_manufacture"
                                                                {!!show_ruler($userItem,'delete_manufacture')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa nhà sản xuất</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_producer"
                                                                {!!show_ruler($userItem,'view_producer')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem nhà cung cấp</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_producer"
                                                                {!!show_ruler($userItem,'add_producer')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm nhà cung cấp</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_producer"
                                                                {!!show_ruler($userItem,'edit_producer')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa nhà cung cấp</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_producer"
                                                                {!!show_ruler($userItem,'delete_producer')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa nhà cung cấp</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_orderImport"
                                                                {!!show_ruler($userItem,'view_orderImport')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xem hóa đơn nhập</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_orderImport"
                                                                {!!show_ruler($userItem,'add_orderImport')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Thêm hóa đơn nhập</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_orderImport"
                                                                {!!show_ruler($userItem,'edit_orderImport')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Sửa hóa đơn nhập</label>
                                                            </div>
                                                            </td>
                                                            <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_orderImport"
                                                                {!!show_ruler($userItem,'delete_orderImport')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck2">Xóa hóa đơn nhập</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_warehouse"
                                                            {!!show_ruler($userItem,'view_warehouse')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Xem kho hàng</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_warehouse"
                                                            {!!show_ruler($userItem,'add_warehouse')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Thêm kho hàng</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_warehouse"
                                                            {!!show_ruler($userItem,'edit_warehouse')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Sửa kho hàng</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_warehouse"
                                                            {!!show_ruler($userItem,'delete_warehouse')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Xóa kho hàng</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="view_product"
                                                            {!!show_ruler($userItem,'view_product')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Xem bài viết</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="add_product"
                                                            {!!show_ruler($userItem,'add_product')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Thêm sản phẩm</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="edit_product"
                                                            {!!show_ruler($userItem,'edit_product')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Sửa sản phẩm</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck2" name="delete_product"
                                                            {!!show_ruler($userItem,'delete_product')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck2">Xóa sản phẩm</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="view_user"
                                                            {!!show_ruler($userItem,'view_user')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck3">Xem danh sách quản trị</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="add_user"
                                                            {!!show_ruler($userItem,'add_user')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck3">Thêm quản trị</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="edit_user"
                                                            {!!show_ruler($userItem,'edit_user')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck3">Sửa quản trị</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck3" name="delete_user"
                                                            {!!show_ruler($userItem,'delete_user')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck3">Xóa quản trị</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_category"
                                                            {!!show_ruler($userItem,'view_category')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem danh mục</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_category"
                                                            {!!show_ruler($userItem,'add_category')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Thêm danh mục</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_category"
                                                            {!!show_ruler($userItem,'edit_category')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Sửa danh mục</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_category"
                                                            {!!show_ruler($userItem,'delete_category')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa danh mục</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_order"
                                                            {!!show_ruler($userItem,'view_order')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem hóa đơn</label>
                                                        </div>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_order"
                                                                {!!show_ruler($userItem,'edit_order')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Xử lý</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_order"
                                                            {!!show_ruler($userItem,'delete_order')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa hóa đơn</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_comment_product"
                                                            {!!show_ruler($userItem,'view_comment_product')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem bình luận sản phẩm</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_comment_product"
                                                                {!!show_ruler($userItem,'add_comment_product')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bình luận sản phẩm</label>
                                                            </div>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_comment_product"
                                                            {!!show_ruler($userItem,'delete_comment_product')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa bình luận sản phẩm</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_comment_blog"
                                                            {!!show_ruler($userItem,'view_comment_blog')!!}
                                                            >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem bình luận bài viết</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_comment_blog"
                                                                {!!show_ruler($userItem,'view_comment_blog')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bình luận bài viết</label>
                                                            </div>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_comment_blog"
                                                            {!!show_ruler($userItem,'delete_comment_blog')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa bình luận bài viết</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_blog"
                                                            {!!show_ruler($userItem,'view_blog')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem bài viết</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                            {{-- <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_blog"
                                                                {!!show_ruler($userItem,'add_blog')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bài viết</label>
                                                            </div> --}}
                                                        </td>
                                                        <td>
                                                            {{-- <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_blog"
                                                                {!!show_ruler($userItem,'edit_blog')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Trả lời bài viết</label>
                                                            </div> --}}
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_blog"
                                                            {!!show_ruler($userItem,'delete_blog')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa bài viết</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_trademark"
                                                            {!!show_ruler($userItem,'view_trademark')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem thương hiệu</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_trademark"
                                                                {!!show_ruler($userItem,'add_trademark')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Thêm thương hiệu</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_trademark"
                                                                {!!show_ruler($userItem,'edit_trademark')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa thương hiệu</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_trademark"
                                                            {!!show_ruler($userItem,'delete_trademark')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa thương hiệu</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_logo"
                                                            {!!show_ruler($userItem,'view_logo')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem Logo</label>
                                                        </div>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_logo"
                                                                {!!show_ruler($userItem,'edit_logo')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa Logo</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">

                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_slide"
                                                            {!!show_ruler($userItem,'view_slide')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem Slide</label>
                                                        </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="add_slide"
                                                                {!!show_ruler($userItem,'add_slide')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Thêm Slide</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_slide"
                                                                {!!show_ruler($userItem,'edit_slide')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa Slide</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="delete_slide"
                                                            {!!show_ruler($userItem,'delete_slide')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xóa Slide</label>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="view_contact"
                                                            {!!show_ruler($userItem,'view_contact')!!}
                                                                >
                                                            <label class="custom-control-label" for="tableDefaultCheck4">Xem trang liên hệ Website</label>
                                                        </div>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input" id="tableDefaultCheck4" name="edit_contact"
                                                                {!!show_ruler($userItem,'edit_contact')!!}
                                                                >
                                                                <label class="custom-control-label" for="tableDefaultCheck4">Sửa trang liên hệ Website</label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <div class="custom-control custom-checkbox">

                                                        </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            <!-- Table  -->

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-light">Lưu thành viên</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@section('script')
    @parent
    <script>
        function changeImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#avatar').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {
            $('#avatar').click(function() {
                $('#img').click();
            });
        });

    </script>
@stop
@endsection
