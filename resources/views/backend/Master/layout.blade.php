<!doctype html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
	<link rel="icon" href="assets/images/favicon-32x32.png') }}" type="image/png" />
	<!--plugins-->
	<link href="{{asset('backend/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
	<link href="{{asset('backend/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
	<link href="{{asset('backend/assets/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
	<!-- loader-->
	<link href="{{asset('backend/assets/css/pace.min.css') }}" rel="stylesheet" />
	<script src="{{asset('backend/assets/js/pace.min.js') }}"></script>
	<!-- Bootstrap CSS -->
	<link href="{{asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;display=swap" rel="stylesheet">
	<link href="{{asset('backend/assets/css/app.css') }}" rel="stylesheet">
	<link href="{{asset('backend/assets/css/main-dev.css') }}" rel="stylesheet">
	{{-- <link href="{{asset('backend/assets/css/style.css') }}" rel="stylesheet"> --}}
	<link href="{{asset('backend/assets/css/icons.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    {{-- <link type="text/css" rel="stylesheet" href="http://example.com/image-uploader.min.css"> --}}
    @yield('style')
	<title>@yield('title')</title>
</head>

<body class="bg-theme bg-theme1">
	<!--wrapper-->
	<div class="wrapper">
		
		<!--start header -->
		<header>
			<div class="topbar d-flex align-items-center">
				<nav class="navbar navbar-expand">
					<div class="mobile-toggle-menu"><i class='bx bx-menu'></i>
					</div>
					<div class="search-bar flex-grow-1">
						<div class="position-relative search-bar-box">
							<input type="text" class="form-control search-control" placeholder="Type to search..."> <span class="position-absolute top-50 search-show translate-middle-y"><i class='bx bx-search'></i></span>
							<span class="position-absolute top-50 search-close translate-middle-y"><i class='bx bx-x'></i></span>
						</div>
					</div>
					<div class="top-menu ms-auto">
						<ul class="navbar-nav align-items-center">
							<li class="nav-item mobile-search-icon">
							</li>
							<li class="nav-item dropdown dropdown-large">
							</li>
							<li class="nav-item dropdown dropdown-large">
								<a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <span class="alert-count">7</span>
									<i class='bx bx-bell'></i>
								</a>
								<div class="dropdown-menu1 dropdown-menu-end">
									<a href="javascript:;">
										<div class="msg-header">
										</div>
									</a>
									<div class="header-notifications-list">
									</div>
								</div>
							</li>
							<li class="nav-item dropdown dropdown-large">
								<a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <span class="alert-count">8</span>
									<i class='bx bx-comment'></i>
								</a>
								<div class="dropdown-menu1 dropdown-menu-end">
									<a href="javascript:;">
									</a>
									<div class="header-message-list">
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="user-box dropdown">
						<a class="d-flex align-items-center nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<img src="{{asset('images/imageAdmin/') }}/{{ Auth::user()->image }}" class="user-img" alt="user avatar">
							<div class="user-info ps-3">
								<p class="user-name mb-0">{{ Auth::user()->name }}</p>
							</div>
						</a>
						<ul class="dropdown-menu dropdown-menu-end">
							<li><a class="dropdown-item" href="{{ route('logout') }}"><i class='bx bx-log-out-circle'></i><span>Đăng xuất</span></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		<!--end header -->
		<!--sidebar wrapper -->
		<div class="sidebar-wrapper" data-simplebar="true">
			<div class="sidebar-header">
				<div>
					<img src="{{asset('backend/assets/images/logo-icon.png') }}" class="logo-icon" alt="logo icon">
				</div>
				<div>
					<h4 class="logo-text">Dashtreme</h4>
				</div>
				<div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
				</div>
			</div>
			<!--navigation-->
			<ul class="metismenu" id="menu">
				<li>
					<a href="javascript:;" class="has-arrow">
						<div class="parent-icon"><i class='bx bx-home-circle'></i>
						</div>
						<div class="menu-title">Dashboard</div>
					</a>
					<ul>
						<li> <a href="{{route('admin.index')}}"><i class="bx bx-right-arrow-alt"></i>Trang chủ</a>
						</li>
						<li> <a href="{{route('admin.category.index')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý danh mục</a>
						</li>
						<li> <a href="{{route('admin.product.index')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý sản phẩm</a>
						</li>
						<li> <a href="{{route('admin_cmt_product')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý bình luận sản phẩm</a>
						</li>
						<li> <a href="{{route('admin.trademark.index')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý thương hiệu</a>
						</li>
						<li> <a href="{{route('admin.warehouses.index')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý kho hàng</a>
						</li>
						<li> <a href="{{route('admin.bloglist')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý bài viết</a>
						</li>
						<li> <a href="{{route('admin.slide')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý Banner</a>
						</li>
						@role('super-admin')
						<li>
							<a href="javascript:;" class="has-arrow">
								<div class="parent-icon"><i class="bx bx-user-circle"></i>
								</div>
								<div class="menu-title">Quản trị</div>
							</a>
							<ul>
								<li> <a href="{{route('admin_user_list')}}"><i class="bx bx-right-arrow-alt"></i>Danh sách</a>
								</li>
								<li> <a href="{{route('admin_user_add')}}"><i class="bx bx-right-arrow-alt"></i>Thêm mới</a>
								</li>
							</ul>
						</li>
						@endrole
						<li> 
							<a href="javascript:;" class="has-arrow">
								<div class="parent-icon"><i class="bx bx-category"></i>
								</div>
								<div class="menu-title">Quản lý đơn hàng</div>
							</a>
							<ul>
								<li> <a href="{{route('admin.order.orderSuccess')}}"><i class="bx bx-right-arrow-alt"></i>Đơn hàng đã bán</a>
								</li>
								<li> <a href="{{route('admin.order.index')}}"><i class="bx bx-right-arrow-alt"></i>Đơn hàng chưa xử lý</a>
							</ul>
						</li>
						<li>
							<a href="javascript:;" class="has-arrow">
								<div class="parent-icon"><i class="bx bx-category"></i>
								</div>
								<div class="menu-title">Quản lý hóa đơn</div>
							</a>
							<ul>
								<li> <a href="{{route('admin.importInvoice.index')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý hóa đơn nhập</a>
								</li>
								{{-- <li> <a href="{{route('admin.exportInvoice.index')}}"><i class="bx bx-right-arrow-alt"></i>Quản lý hóa đơn xuất</a> --}}
							</ul>
						</li>
						<li>
							<a href="javascript:;" class="has-arrow">
								<div class="parent-icon"><i class="bx bx-category"></i>
								</div>
								<div class="menu-title">Thống kê</div>
							</a>
							<ul>
								<li> <a href="{{route('admin.statistical')}}"><i class="bx bx-right-arrow-alt"></i>Thống kê theo ngày</a>
								</li>
							</ul>
						</li>
						<li> <a href="{{route('admin.getLogo')}}"><i class="bx bx-right-arrow-alt"></i>Cấu hình Website</a>
						</li>
					</ul>
				</li>
			</ul>
			<!--end navigation-->
		</div>
		<!--end sidebar wrapper -->		
		
        {{-- content --}}
        @yield('main')
				
		<!--start overlay-->
		<div class="overlay toggle-icon"></div>
		<!--end overlay-->
		<!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
		<!--End Back To Top Button-->
		{{-- <footer class="page-footer">
			<p class="mb-0">Copyright © 2022</p>
		</footer> --}}
	</div>
	<!--end wrapper-->
	<!--start switcher-->
	<div class="switcher-wrapper">
		<div class="switcher-btn"> <i class='bx bx-cog bx-spin'></i>
		</div>
		<div class="switcher-body">
			<div class="d-flex align-items-center">
				<h5 class="mb-0 text-uppercase">Theme Customizer</h5>
				<button type="button" class="btn-close ms-auto close-switcher" aria-label="Close"></button>
			</div>
			<hr/>
			<p class="mb-0">Gaussian Texture</p>
			  <hr>
			  
			  <ul class="switcher">
				<li id="theme1"></li>
				<li id="theme2"></li>
				<li id="theme3"></li>
				<li id="theme4"></li>
				<li id="theme5"></li>
				<li id="theme6"></li>
			  </ul>
               <hr>
			  <p class="mb-0">Gradient Background</p>
			  <hr>
			  
			  <ul class="switcher">
				<li id="theme7"></li>
				<li id="theme8"></li>
				<li id="theme9"></li>
				<li id="theme10"></li>
				<li id="theme11"></li>
				<li id="theme12"></li>
				<li id="theme13"></li>
				<li id="theme14"></li>
				<li id="theme15"></li>
			  </ul>
		</div>
	</div>
	<!--end switcher--><!-- Bootstrap JS -->
	<script src="{{asset('backend/assets/js/bootstrap.bundle.min.js') }}"></script>
	<!--plugins-->
	<script src="{{asset('backend/assets/js/jquery.min.js') }}"></script>
	<script src="{{asset('backend/assets/plugins/simplebar/js/simplebar.min.js') }}"></script>
	<script src="{{asset('backend/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script>
	<script src="{{asset('backend/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>
	<!--app JS-->
	<script src="{{asset('backend/assets/js/app.js') }}"></script>
    
    @stack('scripts')
	</body>

<!-- Mirrored from creatantech.com/demos/codervent/laravel-vertical/index by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Apr 2021 01:10:57 GMT -->
</html>