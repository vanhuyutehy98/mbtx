@extends('backend.Master.layout')
@section('title')
    Cấu hình Website
@endsection
@section('main')
<div class="page-wrapper">
    <div class="page-content">
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Cấu hình</li>
                    </ol>
                </nav>
            </div>
        </div>
        <h6 class="mb-0 text-uppercase">Cấu hình cho Website của bạn</h6>
        {!! show_errors_request($errors, 'image') !!}
        {!! show_errors_request($errors, 'slogan') !!}
        {!! show_errors_request($errors, 'icon') !!}
        {!! show_errors_request($errors, 'content') !!}
        {!! show_errors_request($errors, 'title') !!}
        {!! alert_success(session('thong-bao-thanh-cong')) !!}
        <hr>
        <div class="card">
            <div class="card-body">
                <div class="row row-cols-auto g-3">
                    @foreach ($setting as $item)
                    <div class="col">
                        <button type="button" class="btn btn-dark px-5" data-bs-toggle="modal"
                            data-bs-target="#exampleDangerModal">Logo</button>
                        <div class="modal fade" id="exampleDangerModal" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content bg-dark">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-white">Cấu hình Logo Website</h5>

                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form action="{{ route('admin.logopost', ['id' => $item->id]) }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body text-white">
                                            <div class="mb-3">
                                                <input id="img" type="file" name="image" class="form-control hidden"
                                                    onchange="changeImg(this)">
                                                @if (isset($item->logo))
                                                    <img id="avatar" name="image" class="thumbnail" width="100%"
                                                        height="350px" src="{{asset('images/logo/') }}/{{ $item->logo }}">
                                                @else
                                                    <img id="avatar" name="image" class="thumbnail" width="100%"
                                                        height="350px" src="{{asset('images/import-img.png') }}">
                                                @endif

                                            </div>
                                            <p>Lưu ý: khi thay đổi Logo Website của bạn sẽ thay đổi</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success"
                                                data-bs-dismiss="modal">Hủy</button>
                                            <button type="submit" class="btn btn-danger">Lưu thay đổi</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                  
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
</div>
<script>
    function changeImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).ready(function() {
        $('#avatar').click(function() {
            $('#img').click();
        });
    });

</script>

@endsection
