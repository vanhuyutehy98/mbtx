@extends('backend.Master.layout')
@section('title')
	Sửa danh mục
@endsection
@section('main')
<!--start page wrapper -->
<div class="page-wrapper">
    <div class="page-content">
         <!--breadcrumb-->
         <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Sửa danh mục</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Sửa danh mục sản phẩm</li>
                    </ol>
                </nav>
            </div>
        </div>
        <!--end breadcrumb-->
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <h6 class="mb-0 text-uppercase">Sửa danh mục {{$category->name}}</h6>
                <hr>
                <div class="card border-top border-0 border-4 border-white">
                    <div class="card-body">
                        <div class="border p-4 rounded">
                            @foreach ($errors->all() as $message)
                                <div class="alert border-0 alert-dismissible fade show">
                                    <div class="text-white">{{$message}}</div>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endforeach
                            <form action="{{route('admin.category.update', ['id' => $category->id])}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row mb-3">
                                    <label for="inputEnterYourName" class="col-sm-3 col-form-label">Tên danh mục<span class="requiredLable">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" placeholder="Nhập tên danh mục" value="{{$category->name}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Slug</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="slug" placeholder="Nhập slug danh mục" value="{{$category->name}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Trạng thái</label>
                                    <div class="col-sm-9">
                                        <select class="form-select" name="active">
                                            <option @if ($category->active == 0) selected @endif value="0">Không</option>
                                            <option @if ($category->active == 1) selected @endif value="1">Có</option>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPhoneNo2" class="col-sm-3 col-form-label">Ảnh danh mục</label>
                                    <div class="col-sm-9">
                                        <input class="inport-image-cat" type='file' name="image" onchange="readURL(this);" />
                                        <img class="img_inport-image-cat" id="blah" src="{{asset('images/category/') }}/{{$category->image}}" alt="your image" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-light px-5">Sửa danh mục</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>