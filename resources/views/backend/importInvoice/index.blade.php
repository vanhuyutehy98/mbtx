@extends('backend.Master.layout')
@section('title')
	Danh sách hóa đơn nhập
@endsection
@section('main')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Danh sách hóa đơn nhập</li>
                        </ol>
                    </nav>
                </div>
                <div class="ps-9" style="margin-left: 900px;">
                    <form class="float-lg-end" action="" method="get">
                        <div class="row row-cols-lg-auto g-2">
                            <div class="col-12">
                                <div class="position-relative">
                                    <input type="text" class="form-control ps-5" placeholder="Tìm..." name="search"> <span class="position-absolute top-50 product-show translate-middle-y"><i class="bx bx-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <a href="{{route('admin.importInvoice.create')}}"><h6 class="mb-0 text-uppercase">Thêm hóa đơn nhập</h6></a>
                    <hr/>
                    <div class="card">
                        <div class="card-body">
                            {!! alert_success(session('thong-bao')) !!}
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Mã hóa đơn nhập</th>
                                        <th scope="col">Tên nhà cung cấp</th>
                                        <th scope="col">Mã sản phẩm</th>
                                        <th scope="col">Số lượng</th>
                                        <th scope="col">Giá nhập</th>
                                        <th scope="col">Mã kho</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($importInvoices as $key => $item)
                                    <tr>
                                        <th scope="row">{{ $importInvoices->firstitem() + $key }}</th>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->manufacture->name}}</td>
                                        <td>{{$item->product->id}}</td>
                                        <td>{{$item->numberProduct}}</td>
                                        <td>{{$item->priceImport}}</td>
                                        <td>{{$item->wareHouse->ware_house_name	}}</td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ $importInvoices->links() }}
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page wrapper -->
@endsection