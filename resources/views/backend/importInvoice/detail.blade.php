@extends('backend.Master.layout')
@section('title')
	Chi tiet kho hàng
@endsection
@section('main')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Chi tiet kho hàng</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Chi tiet kho hàng</li>
                        </ol>
                    </nav>
                </div>
                <div class="ps-9" style="margin-left: 900px;">
                    <form class="float-lg-end" action="" method="get">
                        <div class="row row-cols-lg-auto g-2">
                            <div class="col-12">
                                <div class="position-relative">
                                    <input type="text" class="form-control ps-5" placeholder="Tìm..." name="search"> <span class="position-absolute top-50 product-show translate-middle-y"><i class="bx bx-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h6 class="mb-0 text-uppercase">
                        Chi tiet kho hàng: {{$wareHouse->ware_house_name}}
                    </h6>
                    <h6 class="mb-0 text-uppercase">
                        Mã kho hàng: {{$wareHouse->id}}
                    </h6>
                    <hr/>
                    <div class="card">
                        <div class="card-body">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Danh mục hiện có</th>
                                        <th scope="col">Số sản phẩm quản lý</th>
                                        <th scope="col">Số sản phẩm hiện có</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($wareHouse->categories as $key => $item)
                                    <tr>
                                        <th scope="row">{{ $key + 1 }}</th>
                                        <td>
                                            {{$item->name}}
                                        </td>
                                        <td>
                                            {{$item->products->count()}} sản phẩm
                                        </td>
                                        <td>
                                            <select class="form-select">
                                                @foreach ($item->products as $item_prd)
                                                    <option>{{$item_prd->name}} hiện còn {{$item_prd->number_prd}} sản phẩm</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page wrapper -->
@endsection