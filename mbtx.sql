-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 26, 2022 lúc 02:46 PM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mbtx`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `name`, `slug`, `email`, `email_verified_at`, `password`, `level`, `address`, `phone`, `image`, `datetime`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'chu van huy', 'chu-van-huy', 'vanhuyutehy@gmail.com', NULL, '$2y$10$oAPQNPteJudN4AmCsqP1Z./FCV7KJwkNBNOVFV2Mvea6p5rW4K.j6', 0, 'văn giang, hưng yên', '0374970903', 'chu-van-huy-GXciY7HWcN.jpg', '2021-04-20 00:00:00', NULL, NULL, '2022-03-26 06:37:39'),
(2, 'admin', 'admin', 'admin@gmail.com', NULL, '$2y$10$rCilnTQw4TQFoWrJId/D8eSysUwY2ao6yDMXtDiNTCNggJlKjNJ9y', 0, 'văn giang, hưng yên', '0374970903', 'admin-aQJBczfLCb.jpg', '2021-04-20 00:00:00', NULL, NULL, '2022-03-26 06:37:28'),
(3, 'super-admin', 'super-admin', 'superadmin@gmail.com', NULL, '$2y$10$UCXyMyuM1hODfBsNnWIveuDzXQ3/a7GFKICeMLZ6OhHBl4uHdPqxu', 1, 'văn giang, hưng yên', '0374970903', 'super-admin-Ou2moKoL2t.jpg', '2021-04-20 00:00:00', NULL, NULL, '2022-03-26 06:37:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banners`
--

INSERT INTO `banners` (`id`, `cat_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, '1.jpg', '2022-03-26 06:41:07', '2022-03-26 06:41:07'),
(2, 2, '2.jpg', '2022-03-26 06:41:19', '2022-03-26 06:41:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `image`, `title`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Danh muc', 'danh-muc', 'danh-muc-QR.jpg', 'Danh muc', 0, NULL, '2022-03-26 06:42:51'),
(2, 'Danh muc2', 'danh-muc2', 'danh-muc2-eX.jpg', 'Danh muc 2', 0, NULL, '2022-03-26 06:43:11'),
(3, 'Danh muc 3', 'danh-muc-3', 'danh-muc-3-uH.jpg', 'Danh muc 3', 0, NULL, '2022-03-26 06:43:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `commentblog`
--

CREATE TABLE `commentblog` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_admin_id` int(10) UNSIGNED DEFAULT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `state` int(11) DEFAULT NULL,
  `parent` int(11) NOT NULL,
  `name_user_comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_comment` int(11) DEFAULT NULL,
  `comnent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `prd_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `export_invoice`
--

CREATE TABLE `export_invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_prd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `history_price_products`
--

CREATE TABLE `history_price_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `import_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prd_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `history_price_products`
--

INSERT INTO `history_price_products` (`id`, `import_price`, `price`, `prd_id`, `created_at`, `updated_at`) VALUES
(1, NULL, '2222222', 1, '2022-03-26 06:39:43', '2022-03-26 06:39:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image_products`
--

CREATE TABLE `image_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prd_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `image_products`
--

INSERT INTO `image_products` (`id`, `image`, `prd_id`, `created_at`, `updated_at`) VALUES
(1, 'ao-nu-thu-dong-Xd.jpg', 1, NULL, NULL),
(2, 'ao-nu-thu-dong-Za.jpg', 1, NULL, NULL),
(3, 'ao-nu-thu-dong-Wy.jpg', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `import_invoice`
--

CREATE TABLE `import_invoice` (
  `id` int(10) UNSIGNED NOT NULL,
  `numberProduct` int(11) NOT NULL,
  `priceImport` int(11) NOT NULL,
  `manufacture_id` int(10) UNSIGNED NOT NULL,
  `prd_id` int(10) UNSIGNED NOT NULL,
  `ware_house_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `import_invoice`
--

INSERT INTO `import_invoice` (`id`, `numberProduct`, `priceImport`, `manufacture_id`, `prd_id`, `ware_house_id`, `created_at`, `updated_at`) VALUES
(1, 49, 1000, 1, 1, 1, '2022-03-26 06:40:44', '2022-03-26 06:44:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `manufactures`
--

CREATE TABLE `manufactures` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `manufactures`
--

INSERT INTO `manufactures` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'nhà cung cấp', 'nha-cung-cap', NULL, NULL),
(2, 'nhà cung cấp2', 'nha-cung-cap2', NULL, NULL),
(3, 'nhà cung cấp 3', 'nha-cung-cap-3', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `manufacture_product`
--

CREATE TABLE `manufacture_product` (
  `manufacture_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_05_19_083552_create_permission_tables', 1),
(4, '2022_03_13_135439_add_categories_table', 1),
(5, '2022_03_13_140144_add_producer_table', 1),
(6, '2022_03_13_140236_add_manufacture_table', 1),
(7, '2022_03_20_063054_trademark_table', 1),
(8, '2022_03_20_063127_products_table', 1),
(9, '2022_03_20_063232_manufacture_product_table', 1),
(10, '2022_03_20_063329_image_products_table', 1),
(11, '2022_03_20_063407_history_price_products_table', 1),
(12, '2022_03_20_063438_ware_houses_table', 1),
(13, '2022_03_20_063506_import_invoice_table', 1),
(14, '2022_03_20_063531_export_invoice_table', 1),
(15, '2022_03_20_063646_admin_table', 1),
(16, '2022_03_20_063708_comments_table', 1),
(17, '2022_03_20_063743_ware_houses_category_table', 1),
(18, '2022_03_20_100720_order_table', 1),
(19, '2022_03_20_100733_order_detail_table', 1),
(20, '2022_03_26_034755_blogs', 1),
(21, '2022_03_26_034847_comment_blog', 1),
(22, '2022_03_26_084635_create_banner_table', 1),
(23, '2022_03_26_110238_setting', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Model\\AdminAcount', 1),
(1, 'App\\Model\\AdminAcount', 2),
(2, 'App\\Model\\AdminAcount', 1),
(2, 'App\\Model\\AdminAcount', 2),
(3, 'App\\Model\\AdminAcount', 1),
(4, 'App\\Model\\AdminAcount', 1),
(4, 'App\\Model\\AdminAcount', 2),
(5, 'App\\Model\\AdminAcount', 1),
(5, 'App\\Model\\AdminAcount', 2),
(6, 'App\\Model\\AdminAcount', 1),
(6, 'App\\Model\\AdminAcount', 2),
(18, 'App\\Model\\AdminAcount', 2),
(19, 'App\\Model\\AdminAcount', 2),
(20, 'App\\Model\\AdminAcount', 2),
(21, 'App\\Model\\AdminAcount', 2),
(33, 'App\\Model\\AdminAcount', 2),
(34, 'App\\Model\\AdminAcount', 2),
(36, 'App\\Model\\AdminAcount', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(3, 'App\\Model\\AdminAcount', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(18,0) UNSIGNED NOT NULL,
  `state` tinyint(4) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`id`, `name`, `slug`, `address`, `email`, `phone`, `total`, `state`, `note`, `created_at`, `updated_at`) VALUES
(1, 'Huy Van', 'huy-van', 'awd', 'cucxabeng@gmail.com', '0999999998', '2222222', 1, NULL, '2022-03-26 06:44:35', '2022-03-26 06:44:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderdetail`
--

CREATE TABLE `orderdetail` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(18,0) NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orderdetail`
--

INSERT INTO `orderdetail` (`id`, `name`, `price`, `quantity`, `image`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'Ao nu thu dong', '2222222', 1, 'ao-nu-thu-dong-Xd.jpg', 1, '2022-03-26 06:44:35', '2022-03-26 06:44:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(2, 'view_user', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(3, 'view_category', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(4, 'view_order', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(5, 'view_comment_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(6, 'view_comment_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(7, 'view_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(8, 'view_trademark', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(9, 'view_logo', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(10, 'view_slogan', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(11, 'view_image_polyci', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(12, 'view_slide', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(13, 'view_contact', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(14, 'view_warehouse', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(15, 'view_orderImport', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(16, 'view_manufacture', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(17, 'view_producer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(18, 'add_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(19, 'add_user', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(20, 'add_category', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(21, 'add_comment_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(22, 'add_comment_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(23, 'add_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(24, 'add_trademark', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(25, 'add_image_polyci', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(26, 'add_footer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(27, 'add_detail_footer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(28, 'add_slide', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(29, 'add_warehouse', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(30, 'add_orderImport', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(31, 'add_manufacture', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(32, 'add_producer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(33, 'edit_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(34, 'edit_user', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(35, 'edit_category', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(36, 'edit_order', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(37, 'edit_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(38, 'edit_trademark', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(39, 'edit_logo', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(40, 'edit_slogan', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(41, 'edit_image_polyci', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(42, 'edit_footer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(43, 'edit_detail_footer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(44, 'edit_slide', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(45, 'edit_contact', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(46, 'edit_warehouse', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(47, 'edit_orderImport', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(48, 'edit_manufacture', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(49, 'edit_producer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(50, 'delete_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(51, 'delete_user', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(52, 'delete_category', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(53, 'delete_order', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(54, 'delete_comment_product', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(55, 'delete_comment_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(56, 'delete_blog', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(57, 'delete_trademark', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(58, 'delete_image_polyci', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(59, 'delete_footer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(60, 'delete_detail_footer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(61, 'delete_slide', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(62, 'delete_warehouse', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(63, 'delete_orderImport', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(64, 'delete_manufacture', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(65, 'delete_producer', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `producers`
--

CREATE TABLE `producers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `producers`
--

INSERT INTO `producers` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'nhà sản xuất', 'nha-sab-xuat', NULL, NULL),
(2, 'nhà sản xuất2', 'nha-sab-xuat2', NULL, NULL),
(3, 'nhà sản xuất 3', 'nha-sab-xuat-3', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `material` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT 0,
  `state` int(11) NOT NULL DEFAULT 0,
  `describer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(4) DEFAULT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `producer_id` int(10) UNSIGNED NOT NULL,
  `manufacture_id` int(10) UNSIGNED NOT NULL,
  `trademark_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `title`, `style`, `material`, `color`, `size`, `origin`, `price`, `state`, `describer`, `info`, `featured`, `cat_id`, `producer_id`, `manufacture_id`, `trademark_id`, `created_at`, `updated_at`) VALUES
(1, 'Ao nu thu dong', 'ao-nu-thu-dong', NULL, 'Kiểu dáng', 'Chất liệu', 'd', 'Kích cỡ', 'd', 2222222, 1, '<p>awdawddwa</p>', '<p>awdawd</p>', 1, 2, 1, 1, 1, '2022-03-26 06:39:43', '2022-03-26 06:39:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(2, 'admin', 'web', '2022-03-26 06:22:48', '2022-03-26 06:22:48'),
(3, 'super-admin', 'web', '2022-03-26 06:22:49', '2022-03-26 06:22:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 2),
(18, 3),
(19, 2),
(19, 3),
(20, 2),
(20, 3),
(21, 2),
(21, 3),
(22, 2),
(22, 3),
(23, 3),
(24, 3),
(25, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 2),
(33, 3),
(34, 2),
(34, 3),
(35, 2),
(35, 3),
(36, 2),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(44, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(51, 3),
(52, 3),
(53, 3),
(54, 3),
(55, 3),
(56, 3),
(57, 3),
(58, 3),
(59, 3),
(60, 3),
(61, 3),
(62, 3),
(63, 3),
(64, 3),
(65, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `setting`
--

CREATE TABLE `setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `setting`
--

INSERT INTO `setting` (`id`, `logo`, `created_at`, `updated_at`) VALUES
(3, 'virgin7.png', '2022-03-26 06:42:14', '2022-03-26 06:42:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `trademarks`
--

CREATE TABLE `trademarks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `trademarks`
--

INSERT INTO `trademarks` (`id`, `name`, `slug`, `image`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Adidas', 'adidas', 'adidas.png', NULL, '2022-03-26 06:38:05', '2022-03-26 06:38:05'),
(2, 'VIP PRO', 'vip-pro', 'vip-pro.png', NULL, '2022-03-26 06:38:16', '2022-03-26 06:38:16'),
(3, 'VIP PRO 2', 'vip-pro-2', 'vip-pro-2.png', NULL, '2022-03-26 06:38:29', '2022-03-26 06:38:29'),
(4, 'VIP PRO 3', 'vip-pro-3', 'vip-pro-3.png', NULL, '2022-03-26 06:38:38', '2022-03-26 06:38:38'),
(5, 'VIP PRO 4', 'vip-pro-4', 'vip-pro-4.png', NULL, '2022-03-26 06:38:55', '2022-03-26 06:38:55');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `warehouses_category`
--

CREATE TABLE `warehouses_category` (
  `wareHouses_id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `warehouses_category`
--

INSERT INTO `warehouses_category` (`wareHouses_id`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-03-26 06:40:16', '2022-03-26 06:40:16'),
(1, 2, '2022-03-26 06:40:16', '2022-03-26 06:40:16'),
(1, 3, '2022-03-26 06:40:16', '2022-03-26 06:40:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ware_houses`
--

CREATE TABLE `ware_houses` (
  `id` int(10) UNSIGNED NOT NULL,
  `ware_house_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ware_house_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_products` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ware_houses`
--

INSERT INTO `ware_houses` (`id`, `ware_house_code`, `ware_house_name`, `location`, `number_products`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Kho Hưng Yên', 'Văn giang, Hưng yên', NULL, '2022-03-26 06:40:16', '2022-03-26 06:40:16');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_email_unique` (`email`);

--
-- Chỉ mục cho bảng `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_cat_id_foreign` (`cat_id`);

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blogs_title_unique` (`title`),
  ADD UNIQUE KEY `blogs_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Chỉ mục cho bảng `commentblog`
--
ALTER TABLE `commentblog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commentblog_use_admin_id_foreign` (`use_admin_id`),
  ADD KEY `commentblog_blog_id_foreign` (`blog_id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_prd_id_foreign` (`prd_id`);

--
-- Chỉ mục cho bảng `export_invoice`
--
ALTER TABLE `export_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `history_price_products`
--
ALTER TABLE `history_price_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_price_products_prd_id_foreign` (`prd_id`);

--
-- Chỉ mục cho bảng `image_products`
--
ALTER TABLE `image_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_products_prd_id_foreign` (`prd_id`);

--
-- Chỉ mục cho bảng `import_invoice`
--
ALTER TABLE `import_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `import_invoice_manufacture_id_foreign` (`manufacture_id`),
  ADD KEY `import_invoice_prd_id_foreign` (`prd_id`),
  ADD KEY `import_invoice_ware_house_id_foreign` (`ware_house_id`);

--
-- Chỉ mục cho bảng `manufactures`
--
ALTER TABLE `manufactures`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `manufacture_product`
--
ALTER TABLE `manufacture_product`
  ADD KEY `manufacture_product_manufacture_id_foreign` (`manufacture_id`),
  ADD KEY `manufacture_product_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Chỉ mục cho bảng `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderdetail_order_id_foreign` (`order_id`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Chỉ mục cho bảng `producers`
--
ALTER TABLE `producers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_cat_id_foreign` (`cat_id`),
  ADD KEY `products_producer_id_foreign` (`producer_id`),
  ADD KEY `products_manufacture_id_foreign` (`manufacture_id`),
  ADD KEY `products_trademark_id_foreign` (`trademark_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Chỉ mục cho bảng `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `trademarks`
--
ALTER TABLE `trademarks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trademarks_name_unique` (`name`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Chỉ mục cho bảng `warehouses_category`
--
ALTER TABLE `warehouses_category`
  ADD KEY `warehouses_category_warehouses_id_foreign` (`wareHouses_id`),
  ADD KEY `warehouses_category_cat_id_foreign` (`cat_id`);

--
-- Chỉ mục cho bảng `ware_houses`
--
ALTER TABLE `ware_houses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `commentblog`
--
ALTER TABLE `commentblog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `export_invoice`
--
ALTER TABLE `export_invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `history_price_products`
--
ALTER TABLE `history_price_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `image_products`
--
ALTER TABLE `image_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `import_invoice`
--
ALTER TABLE `import_invoice`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `manufactures`
--
ALTER TABLE `manufactures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT cho bảng `producers`
--
ALTER TABLE `producers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `setting`
--
ALTER TABLE `setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `trademarks`
--
ALTER TABLE `trademarks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `ware_houses`
--
ALTER TABLE `ware_houses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `commentblog`
--
ALTER TABLE `commentblog`
  ADD CONSTRAINT `commentblog_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commentblog_use_admin_id_foreign` FOREIGN KEY (`use_admin_id`) REFERENCES `admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_prd_id_foreign` FOREIGN KEY (`prd_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `history_price_products`
--
ALTER TABLE `history_price_products`
  ADD CONSTRAINT `history_price_products_prd_id_foreign` FOREIGN KEY (`prd_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `image_products`
--
ALTER TABLE `image_products`
  ADD CONSTRAINT `image_products_prd_id_foreign` FOREIGN KEY (`prd_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `import_invoice`
--
ALTER TABLE `import_invoice`
  ADD CONSTRAINT `import_invoice_manufacture_id_foreign` FOREIGN KEY (`manufacture_id`) REFERENCES `manufactures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `import_invoice_prd_id_foreign` FOREIGN KEY (`prd_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `import_invoice_ware_house_id_foreign` FOREIGN KEY (`ware_house_id`) REFERENCES `ware_houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `manufacture_product`
--
ALTER TABLE `manufacture_product`
  ADD CONSTRAINT `manufacture_product_manufacture_id_foreign` FOREIGN KEY (`manufacture_id`) REFERENCES `manufactures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `manufacture_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `orderdetail_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_manufacture_id_foreign` FOREIGN KEY (`manufacture_id`) REFERENCES `manufactures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_producer_id_foreign` FOREIGN KEY (`producer_id`) REFERENCES `producers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_trademark_id_foreign` FOREIGN KEY (`trademark_id`) REFERENCES `trademarks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `warehouses_category`
--
ALTER TABLE `warehouses_category`
  ADD CONSTRAINT `warehouses_category_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `warehouses_category_warehouses_id_foreign` FOREIGN KEY (`wareHouses_id`) REFERENCES `ware_houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
