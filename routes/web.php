<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('authenticate', 'Admin\HomeController@SignIn')->name('admin_get_sigin')->middleware('CheckLogout');
Route::post('authenticate', 'Admin\HomeController@SignInPost')->name('admin_post_sigin');
Route::get('logout', 'Admin\HomeController@Logout')->name('logout');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'CheckLogin'], function () {
    Route::get('', 'HomeController@index')->name('admin.index');
    // danh mục
    Route::group(['prefix' => '', 'namespace' => 'Category'], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_category'], function () {
            Route::get('danh-muc.html', 'CategoryController@index')->name('admin.category.index');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_category'], function () {
            Route::get('tao-danh-muc', 'CategoryController@create')->name('admin.category.create');
            Route::post('store-danh-muc', 'CategoryController@store')->name('admin.category.store');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_category'], function () {
            Route::get('sua-danh-muc/{slug}', 'CategoryController@edit')->name('admin.category.edit');
            Route::post('update-danh-muc/{id}', 'CategoryController@update')->name('admin.category.update');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:delete_category'], function () {
            Route::get('xoa-danh-muc/{id}', 'CategoryController@remove')->name('admin.category.remove');
        });
    });
    // sản phẩm
    Route::group(['prefix' => '', 'namespace' => 'Product'], function () {
        // view
        Route::group(['prefix' => '', 'middleware' => 'can:view_product'], function () {
            Route::get('san-pham.html', 'ProductController@index')->name('admin.product.index');
            Route::get('chi-tiet/{slug}',  'ProductController@DetailProduct')->name('admin_prd_detail');
        });
        // add
        Route::group(['prefix' => '', 'middleware' => 'can:add_product'], function () {
            Route::get('tao-san-pham', 'ProductController@create')->name('admin.product.create');
            Route::post('store-san-pham', 'ProductController@store')->name('admin.product.store');
        });
        // edit
        Route::group(['prefix' => '', 'middleware' => 'can:edit_product'], function () {
            Route::get('sua-san-pham/{slug}', 'ProductController@edit')->name('admin.product.edit');
            Route::post('update-san-pham/{id}', 'ProductController@update')->name('admin.product.update');
            Route::post('sua-anh-chi-tiet-san-pham/{id}', 'ProductController@EditImageDetailProductPost')->name('admin_prd_editimagedetailpost');
            Route::get('xoa-anh-chi-tiet-san-pham/{id}', 'ProductController@DeleteImageDetailProductPost')->name('admin_prd_delete_imagedetail');
        });
        // delete
        Route::group(['prefix' => '', 'middleware' => 'can:delete_product'], function () {
            Route::get('xoa-san-pham/{id}', 'ProductController@remove')->name('admin.product.remove');
        });
        // comment
        Route::group(['prefix' => '', 'middleware' => 'can:view_comment_product'], function () {
            Route::get('binh-luan-san-pham.html',  'ProductController@Comment')->name('admin_cmt_product');
            Route::get('binh-luan-san-pham-da-xu-ly.html',  'ProductController@DaXuLy')->name('admin_cmt_ok');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_comment_product'], function () {
            Route::post('binh-luan-san-pham.html',  'ProductController@CommentPost')->name('admin_cmt_productpost')->middleware('can:add_comment_product');
            Route::post('binh-luan-san-pham-da-xu-ly.html',  'ProductController@DaXuLyPost')->middleware('can:add_comment_product');
        });
        Route::get('xoa-binh-luan-san-pham/{id}',  'ProductController@removeCommentProduct')->name('admin_cmt_remove')->middleware('can:delete_comment_product');
    });
    // nhà cung cấp
    Route::group(['prefix' => '', 'namespace' => 'manufacture'], function () {
        Route::get('nha-cung-cap.html', 'ManufactureController@index')->name('admin.manufacture.index')->middleware('can:view_manufacture');
        Route::group(['prefix' => '', 'middleware' => 'can:add_manufacture'], function () {
            Route::get('tao-nha-cung-cap', 'ManufactureController@create')->name('admin.manufacture.create');
            Route::post('store-nha-cung-cap', 'ManufactureController@store')->name('admin.manufacture.store');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_manufacture'], function () {
            Route::get('sua-nha-cung-cap/{slug}', 'ManufactureController@edit')->name('admin.manufacture.edit');
            Route::post('update-nha-cung-cap/{id}', 'ManufactureController@update')->name('admin.manufacture.update');
        });
        Route::get('xoa-nha-cung-cap/{id}', 'ManufactureController@remove')->name('admin.manufacture.remove')->middleware('can:delete_manufacture');
    });
    // nhà sản xuất
    Route::group(['prefix' => '', 'namespace' => 'producer'], function () {
        Route::get('nha-san-xuat.html', 'producerController@index')->name('admin.producer.index')->middleware('can:view_producer');
        Route::group(['prefix' => '', 'middleware' => 'can:add_producer'], function () {
            Route::get('tao-nha-san-xuat', 'producerController@create')->name('admin.producer.create');
            Route::post('store-nha-san-xuat', 'producerController@store')->name('admin.producer.store');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_producer'], function () {
            Route::get('sua-nha-san-xuat/{slug}', 'producerController@edit')->name('admin.producer.edit');
            Route::post('update-nha-san-xuat/{id}', 'producerController@update')->name('admin.producer.update');
        });
        Route::get('xoa-nha-san-xuat/{id}', 'producerController@remove')->name('admin.producer.remove')->middleware('can:delete_producer');
    });
    // giá
    Route::group(['prefix' => '', 'namespace' => 'historyPrice'], function () {
        Route::get('lich-su-gia-san-pham', 'historyPriceController@index')->name('admin.historyPrice.index');
    });
    // mã kho hàng
    Route::group(['prefix' => '', 'namespace' => 'warehouses'], function () {
        Route::get('danh-sach-kho-hang', 'WarehousesController@index')->name('admin.warehouses.index')->middleware('can:view_warehouse');
        Route::group(['prefix' => '', 'middleware' => 'can:add_warehouse'], function () {
            Route::get('them-kho-hang', 'WarehousesController@create')->name('admin.warehouses.create');
        Route::post('add-kho-hang', 'WarehousesController@store')->name('admin.warehouses.store');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_warehouse'], function () {
            Route::get('sua-kho-hang', 'WarehousesController@edit')->name('admin.warehouses.edit');
            Route::post('update-kho-hang/{id}', 'WarehousesController@update')->name('admin.warehouses.update');
        });
       
        Route::get('chi-tiet-kho-hang/{id}', 'WarehousesController@detail')->name('admin.warehouses.detail')->middleware('can:view_warehouse');
        Route::get('xoa-kho-hang/{id}', 'WarehousesController@remove')->name('admin.warehouses.remove')->middleware('can:delete_warehouse');
        
    });
    // quản lý hóa đơn nhập
    Route::group(['prefix' => '', 'namespace' => 'importInvoice'], function () {
        Route::get('danh-sach-hoa-don-nhap', 'importInvoiceController@index')->name('admin.importInvoice.index')->middleware('can:view_orderImport');
        Route::group(['prefix' => '', 'middleware' => 'can:add_orderImport'], function () {
            Route::get('them-hoa-don-nhap', 'importInvoiceController@create')->name('admin.importInvoice.create');
            Route::post('add-hoa-don-nhap', 'importInvoiceController@store')->name('admin.importInvoice.store');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_orderImport'], function () {
            Route::get('sua-hoa-don-nhap', 'importInvoiceController@edit')->name('admin.importInvoice.edit');
            Route::post('update-hoa-don-nhap/{id}', 'importInvoiceController@update')->name('admin.importInvoice.update');
        });
      
        Route::get('chi-tiet-hoa-don-nhap/{id}', 'importInvoiceController@detail')->name('admin.importInvoice.detail')->middleware('can:view_orderImport');
        Route::get('xoa-hoa-don-nhap/{id}', 'importInvoiceController@remove')->name('admin.importInvoice.remove')->middleware('can:delete_orderImport');
    });
    // quản lý hóa đơn xuất
    // Route::group(['prefix' => '', 'namespace' => 'importInvoice'], function () {
    //     Route::get('danh-sach-hoa-don-xuất', 'exportInvoiceController@index')->name('admin.exportInvoice.index');
    //     Route::get('them-hoa-don-xuất', 'exportInvoiceController@create')->name('admin.exportInvoice.create');
    //     Route::post('add-hoa-don-xuất', 'exportInvoiceController@store')->name('admin.exportInvoice.store');
    //     Route::get('sua-hoa-don-xuất', 'exportInvoiceController@edit')->name('admin.exportInvoice.edit');
    //     Route::get('chi-tiet-hoa-don-xuất/{id}', 'exportInvoiceController@detail')->name('admin.exportInvoice.detail');
    //     Route::get('xoa-hoa-don-xuất/{id}', 'exportInvoiceController@remove')->name('admin.exportInvoice.remove');
    //     Route::post('update-hoa-don-xuất/{id}', 'exportInvoiceController@update')->name('admin.exportInvoice.update');
    // });
    // quản lý don hang
    Route::group(['prefix' => '', 'namespace' => 'order'], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_order'], function () {
            Route::get('danh-sach-hoa-don', 'orderController@index')->name('admin.order.index');
            Route::get('tim-hoa-don', 'orderController@search')->name('admin.order.search');
            Route::get('them-hoa-don', 'orderController@create')->name('admin.order.create');
            Route::post('add-hoa-don', 'orderController@store')->name('admin.order.store');
            Route::get('sua-hoa-don', 'orderController@edit')->name('admin.order.edit');
            Route::get('chi-tiet-hoa-don/{id}', 'orderController@detail')->name('admin.order.detail');
            Route::get('danh-sach-hoa-don-da-hoan-thanh', 'orderController@orderSuccess')->name('admin.order.orderSuccess');
        });
        Route::get('update-hoa-don/{id}', 'orderController@update')->name('admin.order.update')->middleware('can:edit_order');
        Route::get('xoa-hoa-don/{id}', 'orderController@remove')->name('admin.order.remove')->middleware('can:delete_order');
    });
    // quản lý thương hiệu
    Route::group(['prefix' => '', 'namespace' => 'trademark'], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:add_trademark'], function () {
            Route::get('them-thuong-hieu', 'trademarkController@create')->name('admin.trademark.create');
            Route::post('add-thuong-hieu', 'trademarkController@store')->name('admin.trademark.store');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_trademark'], function () {
            Route::get('sua-thuong-hieu', 'trademarkController@edit')->name('admin.trademark.edit');
            Route::post('update-thuong-hieu/{id}', 'trademarkController@update')->name('admin.trademark.update');
        });
        Route::get('danh-sach-thuong-hieu', 'trademarkController@index')->name('admin.trademark.index')->middleware('can:view_trademark');
        Route::get('xoa-thuong-hieu/{id}', 'trademarkController@remove')->name('admin.trademark.remove')->middleware('can:delete_trademark');
      
    });
    // quản lý admin acount
    Route::group(['prefix' => '', 'namespace' => 'User', 'middleware' => ['role:super-admin']], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_user'], function () {
            Route::get('danh-sach-quan-tri.html', 'UserController@List')->name('admin_user_list');
            Route::get('chi-tiet-quan-tri/{slug}', 'UserController@Detail')->name('admin_user_detail');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_user'], function () {
            Route::get('them-quan-tri', 'UserController@Add')->name('admin_user_add');
            Route::post('them-quan-tri', 'UserController@AddPost')->name('admin_user_addpost');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_user'], function () {
            Route::get('sua-quan-tri/{slug}', 'UserController@Edit')->name('admin_user_edit');
            Route::post('sua-quan-tri/{id}', 'UserController@EditPost')->name('admin_user_edit_post');
            Route::post('sua-quan-tri-post/{id}', 'UserController@EditDetailPost')->name('admin_user_editDetailpost');
        });
        Route::get('xoa-quan-tri/{id}', 'UserController@Delete')->name('admin_user_delete')->middleware('can:delete_user');
    });
    // quan ly bai viet
    Route::group(['prefix' => '', 'namespace' => 'Blog'], function () {
        Route::group(['prefix' => '', 'middleware' => 'can:view_comment_blog'], function () {
            Route::get('binh-luan-blog-chua-xu-ly.html', 'BLogController@CommentBlogNo')->name('admin.cmtblogno');
            Route::get('binh-luan-blog-da-xu-ly.html', 'BLogController@CommentBlogYes')->name('admin.cmtblogyes');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_comment_blog'], function () {
            Route::post('binh-luan-blog-chua-xu-ly.html', 'BLogController@CommentBlogNoPost');
            Route::post('binh-luan-blog-da-xu-ly.html', 'BLogController@CommentBlogYesPost');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:view_blog'], function () {
            Route::get('danh-sach-bai-viet.html', 'BLogController@List')->name('admin.bloglist');
            Route::get('search', 'BLogController@Search')->name('admin.blogsearch');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:add_blog'], function () {
            Route::get('them-bai-viet', 'BLogController@AddBlog')->name('admin.blogadd');
            Route::post('them-bai-viet', 'BLogController@AddBlogPost')->name('admin.blogaddpost');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_blog'], function () {
            Route::get('sua-bai-viet/{id}/{slug}', 'BLogController@EditBlog')->name('admineditblog');
            Route::post('sua-bai-viet/{id}/{slug}', 'BLogController@EditPostBlog')->name('admin.editpostblog');
        });
        Route::get('remove/{id}', 'BLogController@RemoveBlog')->name('admin.removeblog')->middleware('can:delete_blog');
        Route::get('remove-cmt/{id}', 'BLogController@Remove')->name('admin.removecmt')->middleware('can:delete_comment_blog');
    });
    // quan ly banner
    Route::group(['prefix' => '', 'namespace' => 'Banner'], function () {
        Route::get('setting-slide', 'BannerController@Slide')->name('admin.slide')->middleware('can:view_slide');
        Route::group(['prefix' => '', 'middleware' => 'can:add_slide'], function () {
            Route::post('setting-add-slide', 'BannerController@AddSlide')->name('admin.addslide');
            Route::get('setting-add-slide', 'BannerController@getAddSlide');
        });
        Route::group(['prefix' => '', 'middleware' => 'can:edit_slide'], function () {
            Route::post('setting-edit-slide/{id}', 'BannerController@EditSlide')->name('admin.editslide');
            Route::get('setting-edit-slide/{id}', 'BannerController@getEditSlide');
        });
        Route::get('setting-del-slide/{id}', 'BannerController@delSlide')->name('admin.delslide')->middleware('can:delete_slide');

    });
    // Cau hinh website
    Route::group(['prefix' => '', 'namespace' => 'Setting'], function () {
        // logo
        Route::post('logo-post/{id}', 'SettingController@logopost')->name('admin.logopost')->middleware('can:edit_logo');
        Route::get('logo', 'SettingController@getLogo')->name('admin.getLogo')->middleware('can:view_logo');

    });
    // thống kê theo ngày
    Route::get('thong-ke-doanh-thu-theo-ngay.html', 'HomeController@statistical')->name('admin.statistical');
});

Route::group(['prefix' => '', 'namespace' => 'Frontend'], function () {
    Route::get('', 'HomeController@index')->name('frontend.index');
    Route::get('chi-tiet/{slug}', 'HomeController@detail')->name('frontend.detail');
    Route::get('danh-muc/{slug}', 'HomeController@category')->name('frontend.category');
    Route::get('cua-hang', 'HomeController@shop')->name('frontend.shop');

    Route::get('gio-hang', 'HomeController@buy')->name('frontend.buy');
    Route::post('them-gio-hang', 'HomeController@buycar')->name('frontend.buycar');
    Route::get('updatecart/{rowId}/{qty}', 'HomeController@UpdateCart');
    Route::get('xoa-don-hang/{id}', 'HomeController@RemoveCart')->name('removecart');
    Route::get('them-gio-hang/{slug}', 'HomeController@AddCartShop')->name('addcartshop');



    Route::get('dat-hang', 'HomeController@order')->name('frontend.order');
    Route::post('mua-hang', 'HomeController@orderPost')->name('frontend.orderPost');

    Route::get('thuong-hieu/{slug}', 'HomeController@trademark')->name('frontend.trademark');
    Route::get('bai-viet.html', 'HomeController@post')->name('frontend.post');
    Route::get('chi-tiet-bai-viet/{id}', 'HomeController@postDetail')->name('frontend.postDetail');
    Route::post('{slug}.html', 'HomeController@AddComment')->name('addcmtprd');


});