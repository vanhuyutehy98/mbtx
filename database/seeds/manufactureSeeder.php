<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class manufactureSeeder extends Seeder
{
    public function run()
    {
        DB::table('manufactures')->delete();
        DB::table('manufactures')->insert(
            [
                [
                    'id' => '1',
                    'name' => 'nhà cung cấp',
                    'slug' => 'nha-cung-cap',
                ],
                [
                    'id' => '2',
                    'name' => 'nhà cung cấp2',
                    'slug' => 'nha-cung-cap2',
                ],
                [
                    'id' => '3',
                    'name' => 'nhà cung cấp 3',
                    'slug' => 'nha-cung-cap-3',
                ]
            ]
        );
    }
}
