<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class producerSeeder extends Seeder
{
    public function run()
    {
        DB::table('producers')->delete();
        DB::table('producers')->insert(
            [
                [
                    'id' => '1',
                    'name' => 'nhà sản xuất',
                    'slug' => 'nha-sab-xuat',
                ],
                [
                    'id' => '2',
                    'name' => 'nhà sản xuất2',
                    'slug' => 'nha-sab-xuat2',
                ],
                [
                    'id' => '3',
                    'name' => 'nhà sản xuất 3',
                    'slug' => 'nha-sab-xuat-3',
                ]
            ]
        );
    }
}
