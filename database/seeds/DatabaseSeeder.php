<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(manufactureSeeder::class);
        $this->call(producerSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(Admin::class);
    }
}
