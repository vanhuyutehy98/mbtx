<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert(
            [
                [
                    'id' => '1',
                    'name' => 'Danh muc',
                    'slug' => 'danh-muc',
                    'image' => 'no-img.jpg',
                    'title' => 'Danh muc'
                ],
                [
                    'id' => '2',
                    'name' => 'Danh muc2',
                    'slug' => 'danh-muc2',
                    'image' => 'no-img.jpg',
                    'title' => 'Danh muc 2'
                ],
                [
                    'id' => '3',
                    'name' => 'Danh muc 3',
                    'slug' => 'danh-muc-3',
                    'image' => 'no-img.jpg',
                    'title' => 'Danh muc 3'
                ]
            ]
        );
    }
}
