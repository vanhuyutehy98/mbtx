<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WareHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          // kho hàng
          Schema::create('ware_houses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ware_house_code')->nullable();
            $table->string('ware_house_name')->nullable();
            $table->string('location')->nullable();
            $table->string('number_products')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ware_houses', function (Blueprint $table) {
            //
        });
    }
}
