<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WareHousesCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wareHouses_category', function (Blueprint $table) {
            $table->integer('wareHouses_id')->unsigned();
            $table->foreign('wareHouses_id')->references('id')->on('ware_houses')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wareHouses_category', function (Blueprint $table) {
            //
        });
    }
}
