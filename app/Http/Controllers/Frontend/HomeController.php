<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\carCreate;
use App\Http\Requests\OrderCreate;
use App\Model\Blogs;
use App\Model\Categories;
use App\Model\CommentProduct;
use App\Model\order;
use App\Model\orderDetail;
use App\Model\Products;
use App\Model\trademarks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Str;


class HomeController extends Controller
{
    public function index()
    {
        $data['categories'] = Categories::all();
        $data['categorie_take'] = Categories::all()->take(2);
        $data['trademarks'] = trademarks::all();
        $data['category_active'] = Categories::where('active' , 1)->take(3)->get();
        $data['product_new'] = Products::orderBy('created_at', 'DESC')->take(20)->get();
        $data['product_featured'] = Products::where('featured' , 1)->take(3)->get();
        $data['category_home'] = Categories::orderBy('created_at', 'DESC')->take(2)->get();
        return view('fe.index', $data);
    }
    public function detail($slug)
    {
        $data['product'] = Products::where('slug' , $slug)->first();
        $data['relatedProducts'] = Categories::find($data['product']->cat_id)->products;
        return view('fe.detail', $data);
    }
    public function buycar(carCreate $request)
    {
        $product = Products::find($request->prd_id);
        Cart::add(
            [
                'id' => $product->id,
                'name' => $product->name,
                'qty' => $request->quantity,
                'price' => $product->price,
                'weight' => 0,
                'options' => ['img' => $product->productdetail[0]->image, 'slug' => $product->slug]
            ]
        );
        return redirect()->route('frontend.buy');
    }
    function buy()
    {
        $data['product_new'] = Products::orderBy('created_at', 'DESC')->take(20)->get();
        return view('fe.cart', $data);
    }
    public function UpdateCart( $rowId, $qty)
    {
        Cart::update($rowId, $qty);
    }
    public function RemoveCart($id)
    {
        Cart::remove($id);
        return redirect()->back();
    }
    public function order()
    {
        return view('fe.checkout');
    }
    public function orderPost(OrderCreate $request)
    {
        $customer = new order();
        $customer->name = $request->name;
        $customer->slug = Str::slug($request->name);
        $customer->address = $request->address;

        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->total = Cart::subtotal(0, '', '');
        $customer->state = 0;
        if ($request->order_comments) {
            $customer->note = $request->order_comments;
        }
        $customer->save();
        foreach (Cart::content() as $product) {
            $order = new orderDetail();
            $order->name = $product->name;
            $order->price = $product->price;
            $order->quantity = $product->qty;
            $order->image = $product->options->img;
            $order->order_id = $customer->id;
            $order->save();
        }
        Cart::destroy();
        return view('fe.checkoutsucsess');
    }
    public function AddCartShop($slug)
    {
        $product = Products::where('slug', $slug)->first();
        $qty = 1;
        if ($product->productdetail->count() > 0) {
            $image = $product->productdetail[0]->image;
        }
        else{
            $image = 'no-img.jpg';
        }
        Cart::add([
            'id' => $product->id,
            'name' => $product->name,
            'qty' => $qty,
            'price' => $product->price,
            'weight' => 0,
            'options' => [
               
                'img' => $image,
                'slug' => $product->slug
            ]
        ]);

        return redirect()->route('frontend.buy');
    }
    public function shop(Request $request)
    {
        if ($request->submit_price) {
            $data['products'] = Products::whereBetween('price', [$request->start_price, $request->end_price])->paginate(10);
        }
        else if ($request->search) {
            $data['products'] = Products::where('name', 'like', '%' . $request->search . '%')->paginate(10);
        }
        else if ($request->cat) {
            $cate = Categories::where('slug', $request->cat)->first();
            $data['products'] = $cate->products()->paginate(10);
        }else{
            $data['products'] = Products::paginate(10);
        }
        $data['categories'] = Categories::all();
       return view('fe.shop', $data);
    }
    public function post()
    {
        $data['posts'] = Blogs::all();
       return view('fe.blog', $data);
    }
    public function postDetail($id)
    {
        $data['post'] = Blogs::find($id);
        Carbon::setLocale('vi');
        $data['created_at'] = $data['post']->created_at->diffForHumans();
       return view('fe.blogDetail', $data);
    }
    public function AddComment(Request $request, $id)
    {
       
        $commentBlog = new CommentProduct();
        $commentBlog->comment = $request->comment;
        $commentBlog->name = $request->name;
        $commentBlog->email = $request->email;
        $commentBlog->prd_id  = $request->prd_id;
        $commentBlog->state  = 0;
        $commentBlog->save();
        return redirect()->back();
    }
}
