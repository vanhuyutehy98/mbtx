<?php

namespace App\Http\Controllers\Admin\warehouses;

use App\Http\Controllers\Controller;
use App\Http\Requests\wareHousesCreate;
use App\Http\Requests\wareHousesEdit;
use App\Model\warehouses_category;
use App\Model\Categories;
use App\Model\importInvoice;
use App\Model\Products;
use App\Model\wareHouses;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class WarehousesController extends Controller
{
    public function index()
    {
        $data['wareHouses'] = wareHouses::paginate(10);
        
        return view('backend.warehouses.index', $data);
    }
    public function create()
    {
        $data['category'] = Categories::all();
        return view('backend.warehouses.create', $data);
    }
    public function store(wareHousesCreate $request)
    {
        $wareHouses = new wareHouses();
        $wareHouses->ware_house_name = $request->name;
        $wareHouses->location = $request->location;
        $wareHouses->save();

        
        foreach ($request->cat_id as $key => $value) {
            $wareHouse_category = new warehouses_category();
            $wareHouse_category->wareHouses_id = $wareHouses->id;
            $wareHouse_category->cat_id = $value;
            $wareHouse_category->save();
        }
        return redirect()->route('admin.warehouses.index')->with('thong-bao', 'Thêm mới thành công ' . $request->name);
    }
    public function edit(Request $request)
    {
        $data['warehouse'] = wareHouses::find($request->id);
        $data['category'] = Categories::all();
        return view('backend.warehouses.edit', $data);
    }
    public function update(wareHousesEdit $request, $id)
    {
        $wareHouses = wareHouses::find($id);
        $wareHouses->ware_house_name = $request->name;
        $wareHouses->location = $request->location;
        $wareHouses->save();

        warehouses_category::where('wareHouses_id', $id)->delete();
        foreach ($request->cat_id as $key => $value) {
            $wareHouse_category = new warehouses_category();
            $wareHouse_category->wareHouses_id = $wareHouses->id;
            $wareHouse_category->cat_id = $value;
            $wareHouse_category->save();
        }
        return redirect()->route('admin.warehouses.index')->with('thong-bao', 'Cập nhật thành công ' . $request->name);
    }
    public function remove($id)
    {
        $wareHouses = wareHouses::find($id);
        $wareHouses->delete();
        return redirect()->route('admin.warehouses.index');
    }
    public function detail($id)
    {
        $data['wareHouse'] = wareHouses::find($id);
        // $arr = array();
        // foreach ($data['wareHouse']->categories as $key => $category) {
        //     $arr[]['catName'] = $category->name;
        //     $arr[]['productCount'] = $category->products->count();
        //     foreach ($category->products as $key => $product) {
        //         foreach ($product->importInvoices as $key => $importInvoices) {
        //             if($importInvoices->ware_house_id == $id){
        //                 $arr[]['productList'] = $importInvoices->product->get();
        //             }
        //         }
            
        //     }
        // }
        $data['importInvoices'] = importInvoice::where('ware_house_id', $id)->get();
        // foreach ($importInvoices as $key => $importInvoicesItem) {
        //     dd($importInvoicesItem->product->Categories->name);
        // }
        // dd($importInvoices);
        return view('backend.warehouses.detail', $data);
    }
}
