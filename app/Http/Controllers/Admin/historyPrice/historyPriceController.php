<?php

namespace App\Http\Controllers\Admin\historyPrice;

use App\Http\Controllers\Controller;
use App\Model\history_price_products;
use App\Model\Products;
use Illuminate\Http\Request;

class historyPriceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->search) {
            $prd = Products::where('name', $request->search)->first()->id;
            $data['history'] = history_price_products::where('prd_id', $prd)->paginate(10); 
        }else{
            $data['history'] = history_price_products::paginate(10); 
        }
        
        return view('backend.historyPrice.index', $data);
    }
}
