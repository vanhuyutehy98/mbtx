<?php

namespace App\Http\Controllers\Admin\order;

use App\Http\Controllers\Controller;
use App\Model\importInvoice;
use App\Model\order;
use App\Model\Products;
use App\Model\wareHouses;
use Illuminate\Http\Request;

class orderController extends Controller
{
    public function index()
    {
        $data['customers'] = order::orderBy('created_at', 'DESC')->where('state', '0')->get();
        return view('backend.order.index', $data);
    }
    public function detail(Request $request, $id)
    {
        if ($request->tamp) {
            $data['tamp'] = 1;
        }
        $data['order'] = order::find($id);

        return view('backend.order.detail', $data);
    }
    public function update($id)
    {
        $order = order::find($id);
        // dd($order->OrderDetails);
        foreach ($order->OrderDetails as $key => $value) {
            $product = Products::where('name', $value->name)->first();
            foreach (importInvoice::all() as $key => $importInvoice) {
                if ($importInvoice->prd_id == $product->id && $importInvoice->numberProduct > 0) {
                    $import = importInvoice::find($importInvoice->prd_id);
                    $import->numberProduct = $importInvoice->numberProduct - $value->quantity;
                    $import->save();
                }
                else{
                    return redirect()->back()->with('thong-bao-thanh-cong', 'Sản phẩm ' . $product->name .' trong kho quá ít');
                }
            }

        }

        $order->state = '1';
        $order->save();
        return redirect()->route('admin.order.orderSuccess')->with('thong-bao-thanh-cong', 'Cập nhật trạng thái đơn hàng thành công');
    }
    public function orderSuccess()
    {

        $data['customers'] = order::orderBy('created_at', 'DESC')->where('state', '1')->get();
        return view('backend.order.success', $data);
    }
}
