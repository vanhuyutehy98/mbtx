<?php

namespace App\Http\Controllers\Admin\Banner;

use App\Http\Controllers\Controller;
use App\Model\Banner;
use App\Model\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class BannerController extends Controller
{
    public function Slide()
    {
        $data['slides'] = Banner::paginate(15);
        $data['categories'] = Categories::all();
        return view('backend.setting.slide', $data);
    }
    public function AddSlide(Request $request)
    {
        $banner = new Banner();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            $file_old = public_path('images\\slides\\') . $nameImageValue;
            if (file_exists($file_old) != null) {
                $name = $nameValue . Str::random(2);
                $nameImageValue = $name . '.' . $image->extension();
                $destinationPath = public_path('images\\slides\\');
                $image->move($destinationPath, $nameImageValue);
                $banner->image = $nameImageValue;
            } else {
                $destinationPath = public_path('images\\slides\\');
                $image->move($destinationPath, $nameImageValue);
                $banner->image = $nameImageValue;
            }
            $banner->cat_id = $request->name;
            $banner->save();
        }
        return redirect()->back()->with('thong-bao', 'Đã thêm thành công Slide Website');
    }
    public function EditSlide(Request $request, $id)
    {
        $banner = Banner::find($id);
        if ($request->image != null) {
            $file_old = public_path('images\\slides\\') . $banner->image;
            if (file_exists($file_old) != null && $banner->image != 'no-img.jpg') {
                unlink($file_old);
            }
            $file = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . Str::random(2) . '.' . $file->extension();
            $destinationPath = public_path('images\\slides\\');
            $file->move($destinationPath, $nameImageValue);
            $banner->image = $nameImageValue;
            $banner->cat_id = $request->cat_id;
            $banner->save();
            return redirect()->back()->with('thong-bao', 'Cập nhật thành công');
        } else {
            $banner->image = $banner->image;
            $banner->cat_id = $request->cat_id;
            $banner->save();
            return redirect()->back()->with('thong-bao', 'Cập nhật thành công');
        }
    }
    public function delSlide($id)
    {
        $banner = Banner::find($id);
        $file_old = public_path('images\\slides\\') . $banner->image;
        if (file_exists($file_old) != null && $banner->image != 'no-img.jpg') {
            unlink($file_old);
        }
        $banner->delete();
        return redirect()->back()->with('thong-bao', 'Đã xóa thành công Slide Website');
    }
}
