<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCreate;
use App\Http\Requests\ProductUpdate;
use App\Model\Categories;
use App\Model\CommentProduct;
use App\Model\history_price_products;
use App\Model\image_product;
use App\Model\manufacture;
use App\Model\producer;
use App\Model\Products;
use App\Model\trademarks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index()
    {
        $products = Products::paginate(10);
        return view('backend.product.index', compact('products'));
    }
    public function create()
    {
        $data['category'] = Categories::all();
        $data['producers'] = producer::all();
        $data['manufactures'] = manufacture::all();
        $data['trademarks'] = trademarks::all();
        return view('backend.product.create', $data);
    }
    public function store(ProductCreate $request)
    {
        $product = new Products();
        $product->name = $request->name;
        $product->cat_id = $request->cat_id;
        $product->title = $request->title;
        $product->state = $request->state;
        $product->price = $request->price;
        $product->info = $request->info;
        $product->describer = $request->describer;
        $product->style = $request->style;
        $product->featured = $request->featured;
        $product->material = $request->material;
        $product->color = $request->color;
        $product->size = $request->size;
        $product->origin = $request->origin;
        $product->producer_id = $request->producer_id;
        $product->manufacture_id = $request->manufacture_id;
        $product->slug = Str::slug($request->name);
        $product->trademark_id = $request->trademark_id;
        $product->save();
        if ($request->images) {
            $images = array();
            if ($files = $request->file('images')) {
                foreach ($files as $file) {
                    $nameValue = Str::slug($request->name);
                    $nameImageValue3 = $nameValue . '-' . Str::random(2) . '.' . $file->extension();
                    if ($nameImageValue3 != 'no-img.jpg') {
                        $file_old = public_path('images\\product\\') . $nameImageValue3;
                        if (file_exists($file_old) != null) {
                            unlink($file_old);
                            $destinationPath = public_path('images\\product\\');
                        } else {
                            $destinationPath = public_path('images\\product\\');
                            $file->move($destinationPath, $nameImageValue3);
                        }
                    }
                    $images[] = $nameImageValue3;
                }
            }
            foreach ($images as $value) {
                image_product::insert([
                    'image' => $value,
                    'prd_id' => $product->id
                ]);
            }
        }
        $history_price = new history_price_products();
        $history_price->price = $request->price;
        $history_price->prd_id = $product->id;
        $history_price->save();
        return redirect()->route('admin.product.index')->with('thong-bao', 'Đã thêm thành công sản phẩm ' . $request->name);
    }
    public function edit($slug)
    {
        $data['product'] = Products::where('slug', $slug)->first();
        $data['category'] = Categories::all();
        $data['producers'] = Products::all();
        $data['manufactures'] = manufacture::all();
        $data['img_detail'] = $data['product']->productdetail;
        $data['trademarks'] = trademarks::all();
        return view('backend.product.edit', $data);
    }
    function EditImageDetailProductPost(Request $request, $id)
    {
        $product = Products::find($request->prd_id);
        $images = array();
        foreach ($product->productdetail as $imageDetail) {
            if ($request->imageDetail_id == $imageDetail->id) {
                $images[] = $imageDetail->id;
            }
        }
        foreach ($images as $value) {
            if ($request->$value) {
                $file_old = public_path('images\product\\') . $request->imageDetail_name;
                
                if (file_exists($file_old) != null && $request->imageDetail_name != 'no-img.jpg') {
                    unlink($file_old);
                }
                $image_item = $request->file($value);
                $nameImageValue_3 = $product->slug . '-' . Str::random(2) . '.' . $image_item->extension();

                $destinationPath = public_path('images/product\\');

                $image_item->move($destinationPath,  $nameImageValue_3);
                $image_detai = image_product::find($request->imageDetail_id);
                $image_detai->image = $nameImageValue_3;
                $image_detai->prd_id = $request->prd_id;
                $image_detai->save();
            }
        }
        return redirect()->back()->with('thong-bao-thanh-cong', 'Đã cập nhật ảnh thành công');
    }
    function DeleteImageDetailProductPost($id)
    {
        $imageDetail = Image_Product::find($id);
        $file_old = public_path('product\product_detail\\') . $imageDetail->image;
        if (file_exists($file_old) != null && $imageDetail->image != 'no-img.jpg') {
            unlink($file_old);
        }
        $imageDetail->delete();
        return redirect()->back()->with('thong-bao-thanh-cong', 'Đã xóa ảnh thành công');
    }
    public function update(ProductUpdate $request, $id)
    {
        // dd($request->all());
        $product = Products::find($id);
        $product->name = $request->name;
        $product->cat_id = $request->cat_id;
        $product->title = $request->title;
        $product->state = $request->state;
        $product->price = $request->price;
        $product->info = $request->info;
        $product->describer = $request->describer;
        $product->style = $request->style;
        $product->featured = $request->featured;
        $product->material = $request->material;
        $product->color = $request->color;
        $product->size = $request->size;
        $product->origin = $request->origin;
        $product->producer_id = $request->producer_id;
        $product->manufacture_id = $request->manufacture_id;
        $product->slug = Str::slug($request->name);
        $product->trademark_id = $request->trademark_id;
        $product->save();
        if ($request->images) {
            $images = array();
            if ($files = $request->file('images')) {
                foreach ($files as $file) {
                    $nameValue_4 = Str::slug($request->name);
                    $nameImageValue_3 = $nameValue_4 . '-' . Str::random(5) . '.' . $file->extension();
                    $destinationPath_detail = public_path('images/product\\');
                    $file->move($destinationPath_detail, $nameImageValue_3);
                    $images[] = $nameImageValue_3;
                }
            }
            foreach ($images as $value) {
                image_product::insert([
                    'image' => $value,
                    'prd_id' => $product->id
                ]);
            }
        }
        $history_price = new history_price_products();
        $history_price->price = $request->price;
        $history_price->prd_id = $product->id;
        $history_price->save();
        return redirect()->route('admin.product.index')->with('thong-bao', 'Cập nhật thành công sản phẩm ' . $request->name);
    }
    function remove(Request $request, $id)
    {
        $product = Products::find($id);
        foreach ($product->productdetail as $valueImageDetail) {
            $file_old3 = public_path('images\product\\') . $valueImageDetail->image;
            if (file_exists($file_old3) != null) {
                if ($valueImageDetail->image != 'no-img.jpg') {
                    unlink($file_old3);
                }
            };
        }
        Products::destroy($id);
        return redirect()->back()->with('thong-bao-thanh-cong', 'Đã xóa thành công sản phẩm ' . $request->name);
    }
    function Comment()
    {
        $data['comment_products'] = CommentProduct::where('state', '0')->paginate(20);
        return view('backend.product.comment', $data);
    }
    function CommentPost(Request $request)
    {
        $cmt_find = CommentProduct::find($request->commen_id);
        $cmt_find->state = 1;
        $cmt_find->save();
        $cmt = new CommentProduct();
        $cmt->comment = $request->comment;
        $cmt->name = Auth::user()->name;
        $cmt->email = Auth::user()->email;
        $cmt->state = 1;
        $cmt->prd_id = $request->commen_prd_id;
        $cmt->save();
        return redirect()->back()->with('thong-bao-thanh-cong', 'Đã trả lời bình luận');
    }
    public function removeCommentProduct(Request $request, $id)
    {

        foreach (CommentProduct::all() as $value) {
            if ($value->parent == $id) {
                $value->delete();
            }
        }
        $cmt_find = CommentProduct::find($id);
        $cmt_find->delete();
        if ($request->admin) {
            return redirect()->route('admin_cmt_ok')->with('thong-bao', 'Đã xóa bình luận thành công');
        } else {
            return redirect()->route('admin_cmt_product')->with('thong-bao', 'Đã xóa bình luận thành công');
        }
    }
    function DetailProduct($slug)
    {
        $data['product'] = Products::where('slug', $slug)->first();
        $cat_id = $data['product']->cat_id;
        $data['category'] = Categories::find($cat_id);
        return view('backend.product.detail', $data);
    }
}
