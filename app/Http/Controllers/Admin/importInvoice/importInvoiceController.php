<?php

namespace App\Http\Controllers\Admin\importInvoice;

use App\Http\Controllers\Controller;
use App\Http\Requests\importInvoiceCreate;
use App\Model\importInvoice;
use App\Model\manufacture;
use App\Model\Products;
use App\Model\wareHouses;
use Illuminate\Http\Request;

class importInvoiceController extends Controller
{
    public function index()
    {
        $data['importInvoices'] = importInvoice::paginate(10);
        return view('backend.importInvoice.index', $data);
    }
    public function create()
    {
        $data['wareHouses']= wareHouses::all();
        $data['manufactures']= manufacture::all();
        $data['products']= Products::all();
        return view('backend.importInvoice.create', $data);
    }
    public function store(importInvoiceCreate $request)
    {
        $wareHouses = new importInvoice();
        $wareHouses->manufacture_id  = $request->manufacture_id;
        $wareHouses->prd_id = $request->prd_id;
        $wareHouses->priceImport  = $request->price;
        $wareHouses->numberProduct = $request->number_prd;
        $wareHouses->ware_house_id = $request->wareHouse_id;
        $wareHouses->save();
        return redirect()->route('admin.importInvoice.index')->with('thong-bao', 'Thêm mới thành công');
    }
    // public function edit($slug)
    // {
    //     $category = Categories::where('slug', $slug)->first();
    //     return view('backend.category.edit', compact('category'));
    // }
    // public function update(CategoryUpdate $request, $id)
    // {
    //     $category = Categories::find($id);
    //     if ($request->image != null) {
    //         $file_old = public_path('images\\category\\') . $category->image;
    //         if (file_exists($file_old) != null && $category->image != 'no-img.jpg') {
    //             unlink($file_old);
    //         }
    //         $file = $request->file('image');
    //         $nameValue = Str::slug($request->name);
    //         $nameImageValue = $nameValue . '-' . Str::random(2) . '.' . $file->extension();
    //         $destinationPath = public_path('images\\category\\');
    //         $file->move($destinationPath, $nameImageValue);
    //         $category->image = $nameImageValue;
    //     } else {
    //         $category->image = $category->image;
    //     }
    //     $category->name = $request->name;
    //     $category->slug = Str::slug($request->name);
    //     $category->save();
    //     return redirect()->route('admin.category.index');
    // }
    // public function remove($id)
    // {
    //     $category = Categories::find($id);
    //     $category->delete();
    //     return redirect()->route('admin.category.index');
    // }
}
