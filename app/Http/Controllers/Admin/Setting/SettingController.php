<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\LogoCreate;
use App\Model\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function getLogo()
    {
        $setting = Setting::all();
        return view('backend.setting.setting', compact('setting'));
    }
    public function logopost(LogoCreate $request, $id)
    {
        $setting = Setting::find($id);
        if ($request->image != null) {
            $file_old = public_path('images\\logo\\') . $setting->logo;
            if (file_exists($file_old) != null && $setting->image != 'no-img.jpg') {
                unlink($file_old);
            }
            $file = $request->file('image');
            $nameImageValue = 'virgin7.' . $file->extension();
            $destinationPath = public_path('images\\logo\\');
            $file->move($destinationPath, $nameImageValue);
            $setting->logo = $nameImageValue;
            $setting->state = 1;
        } else {
            $setting->logo = 'no-img.jpg';
            $setting->state = 1;
        }
        $setting->delete();

        $settingNew = new Setting();
        $settingNew->logo = $nameImageValue;
        $settingNew->save();
        return redirect()->back()->with('thong-bao-thanh-cong', 'Cập nhật thành công Logo');
    }
}
