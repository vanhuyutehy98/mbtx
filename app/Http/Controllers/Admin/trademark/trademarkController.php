<?php

namespace App\Http\Controllers\Admin\trademark;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrademarkCreate;
use App\Http\Requests\TrademarkEdit;
use App\Model\trademarks;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class trademarkController extends Controller
{
     public function index()
    {
        $data['trademark'] = trademarks::paginate(10);
        return view('backend.trademark.index', $data);
    }
    public function create()
    {
        return view('backend.trademark.create');
    }
    public function store(TrademarkCreate $request)
    {
        $trademark = new trademarks();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            if ($nameImageValue != 'no-img.jpg') {
                $file_old = public_path('images\\trademark\\') . $nameImageValue;
                if (file_exists($file_old) != null) {
                    unlink($file_old);
                    $destinationPath = public_path('images\\trademark\\');
                    $image->move($destinationPath, $nameImageValue);
                    $trademark->image = $nameImageValue;
                } else {
                    $destinationPath = public_path('images\\trademark\\');
                    $image->move($destinationPath, $nameImageValue);
                    $trademark->image = $nameImageValue;
                }
            } else {
                $trademark->image = $nameImageValue;
            }
        }
        $trademark->name = $request->name;
        $trademark->slug = Str::slug($request->name);
        $trademark->save();
        return redirect()->route('admin.trademark.index')->with('thong-bao', 'Thêm mới thành công ' . $request->name);
    }
    public function edit(Request $request)
    {
        $trademark = trademarks::where('slug', $request->slug)->first();
        return view('backend.trademark.edit', compact('trademark'));
    }
    public function update(TrademarkEdit $request, $id)
    {
        $trademark = trademarks::find($id);
        if ($request->image != null) {
            $file_old = public_path('images\\trademark\\') . $trademark->image;
            if (file_exists($file_old) != null && $trademark->image != 'no-img.jpg') {
                unlink($file_old);
            }
            $file = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '-' . Str::random(2) . '.' . $file->extension();
            $destinationPath = public_path('images\\trademark\\');
            $file->move($destinationPath, $nameImageValue);
            $trademark->image = $nameImageValue;
        } else {
            $trademark->image = $trademark->image;
        }
        $trademark->name = $request->name;
        $trademark->slug = Str::slug($request->name);
        $trademark->save();
        return redirect()->route('admin.trademark.index')->with('thong-bao', 'Cập nhật thành công ' . $request->name);
    }
    public function remove($id)
    {
        $trademark = trademarks::find($id);
        $trademark->delete();
        return redirect()->route('admin.trademark.index');
    }
}
