<?php

namespace App\Http\Controllers\Admin;

use App\Comment_Product;
use App\Http\Controllers\Controller;
use App\Model\AdminAcount;
use App\Model\CommentBlog;
use App\Model\CommentProduct;
use App\Model\order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function SignIn()
    {
        return view('backend.login');
    }
    function SignInPost(Request $request)
    {
        $remember = ($request->remember) ? true : false;
        if ($remember) {
            $auth = Auth::attempt(
                [
                    'email'  => strtolower($request->name),
                    'password'  => $request->password
                ],
                $remember
            );
            if ($auth) {
                return redirect()->route('admin.index');
            } else {
                return redirect()->back()->withInput()->with('thongbao', 'Tài khoản khoặc mật khẩu không chính xác!');
            }

        }else{
            $auth = Auth::attempt(
                [
                    'email'  => strtolower($request->name),
                    'password'  => $request->password
                ],
            );
            if ($auth) {
                Auth::once(['email' => $request->name, 'password' => $request->password]);
                return redirect()->route('admin.index');
            } else {
                return redirect()->back()->withInput()->with('thongbao', 'Tài khoản khoặc mật khẩu không chính xác!');
            }
        }
    }
    public function Logout()
    {
        Auth::logout();
        return redirect()->route('admin_get_sigin');
    }
    public function index()
    {
        $data['total_order'] = order::where('state', 0)->count();
        $data['total_cmt_blog'] = CommentBlog::where('state', 0)->count();
        $data['total_cmt_product'] = CommentProduct::where('state', 0)->count();
        //dd($data['total_cmt_product']);
        $data['total_notif'] = ($data['total_order']+$data['total_cmt_blog']+$data['total_cmt_product'] );
        //dd($data['total_cmt_product']);
        // doanh thu theo ngày
        $data['total_many_day'] = order::where('state','1')->whereDate('updated_at', date('Y-m-d'))->get()->sum('total');
        // doanh thu theo tháng
        $data['currentMonth']=Carbon::now()->format('m');
        $data['total_Month']=order::where('state',1)->whereMonth('updated_at',$data['currentMonth'])->get()->sum('total');
        // doanh thu theo năm
        $data['currentYear']=Carbon::now()->format('Y');
        $data['total_Year']=order::where('state',1)->whereYear('updated_at',$data['currentYear'])->get()->sum('total');
        // doanh thu tuần
        $data['total_Week']=order::where('state',1)->whereBetween('updated_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get()->sum('total');
        // review customer product
        $data['cmt_product']=CommentProduct::where('state',0)->orderBy('created_at', 'DESC')->take(20)->get();
        // order
        $data['orders']=order::where('state',0)->take(20)->get();
        //dd($data['cmt_product']);
        return view('backend.index', $data);
    }
    public function statistical()
    {
        $data['customers'] = order::orderBy('created_at', 'DESC')->where('state', '1')->whereDate('created_at', '>=', Carbon::today()->subDay()->toDateString())->get();
        return view('backend.revenueStatistics.index', $data);
    }
}
