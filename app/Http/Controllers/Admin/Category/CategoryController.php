<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryCreate;
use App\Http\Requests\CategoryUpdate;
use App\Model\Categories;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        $data['category'] = Categories::paginate(10);
        return view('backend.category.index', $data);
    }
    public function create()
    {
        return view('backend.category.create');
    }
    public function store(CategoryCreate $request)
    {
        $category = new Categories();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '.' . $image->extension();
            if ($nameImageValue != 'no-img.jpg') {
                $file_old = public_path('images\\category\\') . $nameImageValue;
                if (file_exists($file_old) != null) {
                    unlink($file_old);
                    $destinationPath = public_path('images\\category\\');
                    $image->move($destinationPath, $nameImageValue);
                    $category->image = $nameImageValue;
                } else {
                    $destinationPath = public_path('images\\category\\');
                    $image->move($destinationPath, $nameImageValue);
                    $category->image = $nameImageValue;
                }
            } else {
                $category->image = $nameImageValue;
            }
        }
        $category->name = $request->name;
        $category->active = $request->active;
        $category->slug = Str::slug($request->name);
        $category->save();
        return redirect()->route('admin.category.index')->with('thong-bao', 'Thêm thành công danh mục ' . $request->name);
    }
    public function edit($slug)
    {
        $category = Categories::where('slug', $slug)->first();
        return view('backend.category.edit', compact('category'));
    }
    public function update(CategoryUpdate $request, $id)
    {
        $category = Categories::find($id);
        if ($request->image != null) {
            $file = $request->file('image');
            $nameValue = Str::slug($request->name);
            $nameImageValue = $nameValue . '-' . Str::random(2) . '.' . $file->extension();
            $destinationPath = public_path('images\\category\\');
            $file->move($destinationPath, $nameImageValue);
            $category->image = $nameImageValue;
        } else {
            $category->image = $category->image;
        }
        $category->name = $request->name;
        $category->active = $request->active;
        $category->slug = Str::slug($request->name);
        $category->save();
        return redirect()->route('admin.category.index')->with('thong-bao', 'Cập nhật thành công danh mục ' . $request->name);
    }
    public function remove($id)
    {
        $category = Categories::find($id);
        $category->delete();
        return redirect()->route('admin.category.index');
    }
}
