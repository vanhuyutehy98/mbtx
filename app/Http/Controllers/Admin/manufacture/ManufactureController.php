<?php

namespace App\Http\Controllers\Admin\manufacture;

use App\Http\Controllers\Controller;
use App\Http\Requests\ManufactureCreate;
use App\Http\Requests\ManufactureEdit;
use App\Model\manufacture;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ManufactureController extends Controller
{
    public function index()
    {
        $data['manufacture'] = manufacture::paginate(1);
        return view('backend.manufacture.index', $data);
    }
    public function create()
    {
        return view('backend.manufacture.create');
    }
    public function store(ManufactureCreate $request)
    {
        $manufacture = new manufacture();
        $manufacture->name = $request->name;
        $manufacture->slug = Str::slug($request->name);
        $manufacture->save();
        return redirect()->route('admin.manufacture.index')->with('thong-bao', 'Thêm mới thành công ' . $request->name);
    }
    public function edit($slug)
    {
        $manufacture = manufacture::where('slug', $slug)->first();
        return view('backend.manufacture.edit', compact('manufacture'));
    }
    public function update(ManufactureEdit $request, $id)
    {
        $manufacture = manufacture::find($id);
        $manufacture->name = $request->name;
        $manufacture->slug = Str::slug($request->name);
        $manufacture->save();
        return redirect()->route('admin.manufacture.index')->with('thong-bao', 'Cập nhật thành công ' . $request->name);
    }
    public function remove($id)
    {
        $manufacture = manufacture::find($id);
        $manufacture->delete();
        return redirect()->route('admin.manufacture.index');
    }
}
