<?php

namespace App\Http\Controllers\Admin\producer;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProducerCreate;
use App\Http\Requests\ProducerEdit;
use App\Http\Requests\ProductCreate;
use App\Model\producer;
use App\Model\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class producerController extends Controller
{
    public function index()
    {
        $data['producer'] = producer::paginate(1);
        return view('backend.producer.index', $data);
    }
    public function create()
    {
        return view('backend.producer.create');
    }
    public function store(ProducerCreate $request)
    {
        // dd($request->all());
        $producer = new producer();
        $producer->name = $request->name;
        $producer->slug = Str::slug($request->name);
        $producer->save();
        return redirect()->route('admin.producer.index')->with('thong-bao', 'Thêm mới thành công ' . $request->name);
    }
    public function edit($slug)
    {
        $producer = producer::where('slug', $slug)->first();
        return view('backend.producer.edit', compact('producer'));
    }
    public function update(ProducerEdit $request, $id)
    {
        $producer = producer::find($id);
        $producer->name = $request->name;
        $producer->slug = Str::slug($request->name);
        $producer->save();
        return redirect()->route('admin.producer.index')->with('thong-bao', 'Cập nhật thành công ' . $request->name);
    }
    public function remove($id)
    {
        $producer = producer::find($id);
        $producer->delete();
        return redirect()->route('admin.producer.index');
    }
}
