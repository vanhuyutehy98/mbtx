<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:products,name,'.$this->id,
            'cat_id' => 'required',
            'price' => 'required',
            'style' => 'required',
            'material' => 'required',
            'color' => 'required',
            'size' => 'required',
            'origin' => 'required',
            'producer_id' => 'required',
            'manufacture_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên nhà sản xuất đang để trống',
            'name.unique.required' => 'Tên nhà sản xuất đã tồn tại',
            'cat_id.required' => 'Tên danh mục đang để trống',
            'price.required' => 'giá đang để trống',
            'style.required' => 'kiểu dáng đang để trống',
            'material.required' => 'Tên chất liệu đang để trống',
            'color.required' => 'Màu đang để trống',
            'size.required' => 'kích cỡ đang để trống',
            'origin.required' => 'Suất xứ đang để trống',
            'producer_id.size.required' => 'Nhà cung cấp đang để trống',
            'manufacture_id.origin.required' => 'Nhà sản xuất đang để trống',
        ];
    }
}
