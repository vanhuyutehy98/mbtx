<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class wareHousesEdit extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:ware_houses,ware_house_name,'.$this->id,

        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên kho hàng đang để trống',
            'location.required' => 'Vị trí kho hàng đang để trống',
            'cat_id.required' => 'Danh mục kho hàng đang để trống',
            'name.unique' => 'Tên kho hàng đã tồn tại',
        ];
    }
}
