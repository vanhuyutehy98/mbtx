<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:admin',
            'phone' => 'required',
            'address' => 'required',
            'datetime' => 'required',
            'email' => 'required|unique:admin',
            'password' => 'required',
            'image' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên quản trị đang để trống',
            'name.unique' => 'Tên quản trị đã tồn tại',
            'email.unique' => 'Email quản trị đã tồn tại',
            'phone.required' => 'Số điện thoại đang để trống',
            'address.required' => 'Địa chỉ đang để trống',
            'datetime.required' => 'Ngày sinh đang để trống',
            'email.required' => 'Email đang để trống',
            'password.required' => 'Mật khẩu đang để trống',
            'image.required' => 'Ảnh đang để trống',

        ];
    }
}
