<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class importInvoiceEdit extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number_prd' => 'required',
            'price' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'number_prd.required' => 'Số lượng đang để trống',
            'price.required' => 'Giá đã tồn tại',
        ];
    }
}
