<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEdit extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|unique:admin,name,'.$this->id,

            'phone' => 'required',
            'address' => 'required',
            'datetime' => 'required',
            'email' => 'required|unique:admin,email,'.$this->id,
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên quản trị đang để trống',
            'name.unique' => 'Tên quản trị đã tồn tại',
            'phone.required' => 'Số điện thoại đang để trống',
            'address.required' => 'Địa chỉ đang để trống',
            'datetime.required' => 'Ngày sinh đang để trống',
            'email.required' => 'Email đang để trống',
            'email.unique' => 'Email quản trị đã tồn tại',
        ];
    }
}
