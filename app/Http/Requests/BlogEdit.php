<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogEdit extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'info' => 'required',
            'body' => 'required',
            'image' => 'required',

        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề đang để trống',
            'info.required' => 'Giới thiệu đang để trống',
            'body.required' => 'Nội dung đang để trống',
            'image.required' => 'Ảnh bìa đang để trống',
        ];
    }
}
