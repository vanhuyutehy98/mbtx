<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LogoCreate extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'image.required' => 'Logo đang để trống',
        ];
    }
}
