<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table='products';
    public function Categories()
    {
        return $this->belongsTo(Categories::class, 'cat_id', 'id');
    }
    public function trademark()
    {
        return $this->belongsTo(trademarks::class, 'trademark_id', 'id');
    }
    public function Manufacture()
    {
        return $this->belongsTo(manufacture::class, 'manufacture_id', 'id');
    }
    public function Producer()
    {
        return $this->belongsTo(producer::class, 'producer_id', 'id');
    }
    public function productdetail()
    {
        return $this->hasMany(image_product::class, 'prd_id', 'id');
    }
    public function historyPrice()
    {
        return $this->hasMany(history_price_products::class, 'prd_id', 'id');
    }
    public function importInvoices()
    {
        return $this->hasMany(importInvoice::class, 'prd_id', 'id');
    }
    public function comments()
    {
        return $this->hasMany(CommentProduct::class, 'prd_id', 'id');
    }
}
