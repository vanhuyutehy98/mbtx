<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CommentProduct extends Model
{
    protected $table='comments';
    public function product()
    {
        return $this->belongsTo(Products::class, 'prd_id', 'id');
    }
    
}
