<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table='order';
    public function OrderDetails()
    {
        return $this->hasMany(orderDetail::class,'order_id', 'id');
    }
    
}
