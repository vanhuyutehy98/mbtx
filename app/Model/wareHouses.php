<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class wareHouses extends Model
{
    protected $table='ware_houses';
    public function categories()
    {
        return $this->belongsToMany(Categories::class, 'warehouses_category', 'wareHouses_id', 'cat_id');
    }
}
