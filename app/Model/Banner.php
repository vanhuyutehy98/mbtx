<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    public function Categories()
    {
        return $this->belongsTo(Categories::class, 'cat_id', 'id');
    }
}
