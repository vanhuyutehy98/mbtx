<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    protected $table='blogs';
    public function commentBlog()
    {
        return $this->hasMany(CommentBlog::class, 'blog_id', 'id');
    }
}
