<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class history_price_products extends Model
{
    protected $table='history_price_products';
   
    protected $fillable = [
        'updated_at',
        'created_at',
        'account_id',
        'prd_id ',
        'price',
        'import_price',
    ];
    public function history()
    {
        return $this->belongsTo(Products::class, 'prd_id', 'id');
    }
    public function CarbonLocalecreated_at()
    {
        Carbon::setLocale('vi');

        return $this->created_at->diffForHumans();
    }
    public function CarbonLocaleupdated_at()
    {
        Carbon::setLocale('vi');

        return $this->updated_at->diffForHumans();
    }
}
