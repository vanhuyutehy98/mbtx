<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class importInvoice extends Model
{
    protected $table='import_invoice';
    
    public function manufacture()
    {
        return $this->belongsTo(manufacture::class, 'manufacture_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo(Products::class, 'prd_id', 'id');
    }
    public function wareHouse()
    {
        return $this->belongsTo(wareHouses::class, 'ware_house_id', 'id');
    }
}
