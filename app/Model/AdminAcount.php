<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
class AdminAcount extends Authenticatable
{  
    use HasRoles;
    protected $table='admin';
    protected $guard_name = 'web';
    protected $fillable = [
        'email', 'password', 'role_id'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
