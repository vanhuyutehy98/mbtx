<?php

namespace App\Providers;

use App\Model\Banner as ModelBanner;
use App\Model\Categories;
use App\Model\Setting as ModelSetting;
use Illuminate\Support\ServiceProvider;
use App\Models\{Banner, Footers, Image_policy, Setting};
class ViewSiteProviderService extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('fe.layout.master', function ($view) {
            //pass the data to the view
            $view
                ->with('setting', ModelSetting::all())
                ->with('categories', Categories::all())
                
                // ->with('footer', Footers::where('state', '1')->first())
                // ->with('footer_2', Footers::where('state', '2')->first())
                // ->with('footer_3', Footers::where('state', '3')->first());
                ->with('banners', ModelBanner::all());
        });
    }
}
